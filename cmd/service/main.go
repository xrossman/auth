package main

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/apartstech/infrastructure.git/log"
	pub "gitlab.com/apartstech/layer2/notifications.git/pkg/publisher"
	sub "gitlab.com/apartstech/layer2/notifications.git/pkg/subscriber"

	"gitlab.com/apartstech/layer2/auth.git/v1/app/router"
	"gitlab.com/apartstech/layer2/auth.git/v1/cfg"
	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/grpc"
	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/validation"
)

func main() {
	log.Debug().Msg("initializing configuration")
	if err := cfg.Init(); err != nil {
		log.Fatal().Err(err).Msgf("can't init configuration")
	}

	log.Debug().Msg("ok")

	// GRPC Connection
	log.Debug().Msg("initializing grpc connection")
	if err := grpc.Init(); err != nil {
		log.Fatal().Err(err).Msgf("can't initialize grpc connection")
	}

	log.Debug().Msg("ok")

	defer grpc.CloseConnections()

	// RabbitMQ Connection
	if cfg.Store.MessageQueue.Enabled {
		log.Debug().Msg("initializing message queue connection")
		keys := []sub.QueueRouteKey{
			sub.RegistrationConfirmationQueueName,
		}

		time.Sleep(time.Minute)
		if err := pub.Init(cfg.Store.MessageQueue, keys); err != nil {
			log.Fatal().Err(err).Msgf("can't initialize message queue connection")
		}

		log.Debug().Msg("ok")
	}

	// Validator
	log.Debug().Msg("initializing validator")
	validation.Init()
	log.Debug().Msg("ok")

	// Router
	r := router.InitRouter()
	s := &http.Server{
		Addr:           fmt.Sprintf(":%d", cfg.Store.WS.Port),
		Handler:        r,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	// Start Server
	go func() {
		if err := s.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatal().Err(err).Msgf("service stopped with error")
		}
	}()

	log.Debug().Msgf("listening on %s", cfg.Store.WS.URL())

	select {}
}
