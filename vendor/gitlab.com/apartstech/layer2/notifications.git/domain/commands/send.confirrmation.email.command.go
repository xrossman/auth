package commands

import (
	"encoding/json"

	"github.com/streadway/amqp"
	"gitlab.com/apartstech/infrastructure.git/log"

	"gitlab.com/apartstech/layer2/notifications.git/cfg"
	"gitlab.com/apartstech/layer2/notifications.git/pkg/mailer"
	"gitlab.com/apartstech/layer2/notifications.git/pkg/messagemodels"
)

func SendRegistrationConfirmCommand(m amqp.Delivery) {
	var message messagemodels.RegistrationConfirmationEmailMessage

	if err := json.Unmarshal(m.Body, &message); err != nil {
		log.Print("can't unmarshal json ", err)
		if err := m.Nack(false, true); err != nil {
			log.Print("can't Nack message ", err)
		}
		return
	}

	template, err := mailer.ReadTemplate(message)
	if err != nil {
		log.Debug().Msgf("can't read template %v", err)

		if err := m.Nack(false, true); err != nil {
			log.Debug().Msgf("can't Nack message %v", err)
		}

		return
	}

	options := mailer.MailOptions{
		mailer.FromMailOption:    cfg.Store.Mail.DefaultFrom,
		mailer.ToMailOption:      message.To,
		mailer.SubjectMailOption: "Registration Confirmation",
		mailer.BodyMailOption:    template,
	}

	err = mailer.SendMail(options)
	if err != nil {
		log.Debug().Msgf("can't send email to %s:%d: %s", cfg.Store.Mail.Host, cfg.Store.Mail.Port, err)
		if err := m.Nack(false, true); err != nil {
			log.Debug().Msgf("can't Nack message %v", err)
		}

		return
	}

	if err := m.Ack(false); err != nil {
		log.Debug().Msgf("can't Ack message %v", err)
	}
}
