package commands

import (
	"encoding/json"

	"github.com/streadway/amqp"
	"gitlab.com/apartstech/infrastructure.git/log"

	"gitlab.com/apartstech/layer2/notifications.git/cfg"
	"gitlab.com/apartstech/layer2/notifications.git/pkg/mailer"
	"gitlab.com/apartstech/layer2/notifications.git/pkg/messagemodels"
)

func SendTestEmailCommand(m amqp.Delivery) {
	var message messagemodels.TestEmailMessage

	if err := json.Unmarshal(m.Body, &message); err != nil {
		if err := m.Nack(false, true); err != nil {
			log.Debug().Msgf("can't Nack message %v", err)
		}
		return
	}

	template, err := mailer.ReadTemplate(message)
	if err != nil {
		if err := m.Nack(false, true); err != nil {
			log.Debug().Msgf("can't Nack message %v", err)
		}
		return
	}

	options := mailer.MailOptions{
		mailer.FromMailOption:    cfg.Store.Mail.DefaultFrom,
		mailer.ToMailOption:      message.To,
		mailer.SubjectMailOption: "subject",
		mailer.BodyMailOption:    template,
	}

	err = mailer.SendMail(options)
	if err != nil {
		if err := m.Nack(false, true); err != nil {
			log.Debug().Msgf("can't Nack message %v", err)
		}
		return
	}

	if err := m.Ack(false); err != nil {
		log.Debug().Msgf("can't Nack message %v", err)
	}
}
