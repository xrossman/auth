package cfg

import (
	"fmt"

	config "github.com/microparts/configuration-golang"
	"gopkg.in/yaml.v2"
)

type configStore struct {
	Rabbit RabbitCfg  `yaml:"rabbit"`
	Mail   mailCfg    `yaml:"mail"`
	WS     serviceCfg `yaml:"ws"`
}

type serviceCfg struct {
	Schema         string `yaml:"schema"`
	Host           string `yaml:"host"`
	Port           uint   `yaml:"port"`
	PublicURL      string `yaml:"public_url"`
	ServiceName    string `yaml:"service_name"`
	ServiceVersion string `yaml:"service_version"`
}

type RabbitCfg struct {
	Host string `yaml:"host"`
	Port int    `yaml:"port"`
	User string `yaml:"user"`
	Pass string `yaml:"pass"`
	Enabled bool `yaml:"enabled"`
}

func (rmq RabbitCfg) ConnURL() string {
	return fmt.Sprintf("amqp://%s:%s@%s:%d", rmq.User, rmq.Pass, rmq.Host, rmq.Port)
}

type mailCfg struct {
	Host        string `yaml:"host"`
	Port        int    `yaml:"port"`
	Name        string `yaml:"name"`
	Pass        string `yaml:"pass"`
	TLS         bool   `yaml:"tls"`
	DefaultFrom string `yaml:"default_from"`
	TemplateDir string `yaml:"template_dir"`
	DefaultURL  string `yaml:"default_url"`
}

var Store *configStore

func Init() error {
	path := config.GetEnv("CONFIG_PATH", "")
	cfgBytes, err := config.ReadConfigs(path)
	if err != nil {
		return fmt.Errorf("can't read config files: %s", err)
	}
	err = yaml.Unmarshal(cfgBytes, &Store)
	if err != nil {
		return fmt.Errorf("can't unmarshal yaml: %s", err)
	}

	return nil
}
