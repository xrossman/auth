package mailer

const (
	ToMailOption      = MailOption("To")
	FromMailOption    = MailOption("From")
	SubjectMailOption = MailOption("Subject")
	BodyMailOption    = MailOption("Body")
)

type MailOption string

type MailOptions map[MailOption]string

func (o MailOption) String() string {
	return string(o)
}

type Notifier interface {
	TemplateName() string
	TemplateType() string
	Ads() string
}
