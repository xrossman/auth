package mailer

import (
	"fmt"

	"gitlab.com/apartstech/layer2/notifications.git/cfg"
	"gopkg.in/gomail.v2"
)

var d *gomail.Dialer

func Init() {
	fmt.Println(cfg.Store.Mail.Host, cfg.Store.Mail.Port, cfg.Store.Mail.Name, cfg.Store.Mail.Pass)
	d = gomail.NewDialer(cfg.Store.Mail.Host, cfg.Store.Mail.Port, cfg.Store.Mail.Name, cfg.Store.Mail.Pass)
}

func SendMail(options MailOptions) error {
	m := gomail.NewMessage()
	for option, value := range options {
		switch option {
		case ToMailOption, FromMailOption, SubjectMailOption:
			m.SetHeader(option.String(), value)
		case BodyMailOption:
			m.SetBody("text/html", value)
		}
	}
	return d.DialAndSend(m)
}
