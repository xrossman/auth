package mailer

import (
	"bytes"
	"fmt"

	"html/template"

	"gitlab.com/apartstech/layer2/notifications.git/cfg"
)

func ReadTemplate(data Notifier) (string, error) {
	funcMap := template.FuncMap{
		"safe": func(s string) template.URL {
			return template.URL(s)
		},
	}
	tplBuffer := bytes.Buffer{}

	err := template.Must(template.New(data.TemplateName()).
		Funcs(funcMap).
		ParseFiles(fmt.Sprintf("%s/%s/%s", cfg.Store.Mail.TemplateDir, data.TemplateType(), data.TemplateName()))).
		Execute(&tplBuffer, data)
	return string(tplBuffer.Bytes()), err
}
