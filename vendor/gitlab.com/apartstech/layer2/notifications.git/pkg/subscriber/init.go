package subscriber

import (
	"github.com/streadway/amqp"
	"gitlab.com/apartstech/infrastructure.git/log"

	"gitlab.com/apartstech/layer2/notifications.git/cfg"
	"gitlab.com/apartstech/layer2/notifications.git/domain/commands"
)

type QueueRouteKey string
type ChannelName string

func (qn QueueRouteKey) String() string {
	return string(qn)
}

const (
	DefaultExchangeName = "default"

	RegistrationConfirmationQueueName QueueRouteKey = "registration.confirmation"
	EmailTestQueueName                QueueRouteKey = "email.test"
)

var (
	rmqConn     *amqp.Connection
	amqpChannel *amqp.Channel
	Keys        = []QueueRouteKey{
		EmailTestQueueName,
		RegistrationConfirmationQueueName,
	}
)

func Init() error {
	var err error

	rmqConn, err = amqp.Dial(cfg.Store.Rabbit.ConnURL())
	if err != nil {
		return err
	}

	stop := make(chan bool)
	amqpChannel, err = rmqConn.Channel()
	if err != nil {
		rmqConn.Close()
		return err
	}

	defer amqpChannel.Close()

	err = amqpChannel.ExchangeDeclare(
		DefaultExchangeName,
		amqp.ExchangeDirect,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	q, err := amqpChannel.QueueDeclare("", true, false, false, false, nil)
	if err != nil {
		Close()

		return err
	}

	for _, name := range Keys {
		err = amqpChannel.QueueBind(q.Name, name.String(), DefaultExchangeName, false, nil)
		if err != nil {
			return err
		}
	}

	messageChannel, err := amqpChannel.Consume(
		q.Name,
		"",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	log.Debug().Msgf("message channel %s created", q.Name)

	go listen(messageChannel)

	<-stop

	return nil
}

func listen(ch <-chan amqp.Delivery) {
	for m := range ch {
		log.Debug().Msgf("message %s %s received: %s", m.Type, m.RoutingKey, m.Body)

		switch m.RoutingKey {
		case EmailTestQueueName.String():
			log.Debug().Msgf("message received: %s", EmailTestQueueName)
			go commands.SendTestEmailCommand(m)
		case RegistrationConfirmationQueueName.String():
			log.Debug().Msgf("message received: %s", RegistrationConfirmationQueueName)
			go commands.SendRegistrationConfirmCommand(m)
		default:
			log.Debug().Msgf("m: %v", m)
		}
	}
}

func Close() error {
	err := amqpChannel.Close()
	if err != nil {
		return err
	}

	return rmqConn.Close()
}
