package publisher

import (
	"encoding/json"

	"github.com/streadway/amqp"
	"gitlab.com/apartstech/infrastructure.git/log"

	"gitlab.com/apartstech/layer2/notifications.git/cfg"
	"gitlab.com/apartstech/layer2/notifications.git/pkg/messagemodels"
	"gitlab.com/apartstech/layer2/notifications.git/pkg/subscriber"
)

var (
	conn        *amqp.Connection
	amqpChannel *amqp.Channel
)

func Init(cfg cfg.RabbitCfg, names []subscriber.QueueRouteKey) error {
	var err error
	conn, err = amqp.Dial(cfg.ConnURL())
	if err != nil {
		return err
	}

	amqpChannel, err = conn.Channel()
	if err != nil {
		return err
	}

	err = amqpChannel.ExchangeDeclare(
		subscriber.DefaultExchangeName,
		amqp.ExchangeDirect,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	return nil
}

func Close() error {
	err := amqpChannel.Close()
	if err != nil {
		return err
	}

	return conn.Close()
}

func AddTestEmailMessage(message messagemodels.TestEmailMessage) error {
	body, err := json.Marshal(message)
	if err != nil {
		return err
	}

	err = amqpChannel.Publish(
		subscriber.DefaultExchangeName,
		subscriber.EmailTestQueueName.String(),
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        body,
		})

	if err != nil {
		log.Fatal().Msgf("Error publishing message: %s", err)
		return err
	}

	return nil
}

func AddRegistrationConfirmationMessage(message messagemodels.RegistrationConfirmationEmailMessage) error {
	body, err := json.Marshal(message)
	if err != nil {
		return err
	}
	
	if conn == nil || amqpChannel == nil {
		log.Debug().Msg("ampq connection or channel is nil")
		return nil
	}

	err = amqpChannel.Publish(
		subscriber.DefaultExchangeName,
		subscriber.RegistrationConfirmationQueueName.String(),
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        body,
		})

	if err != nil {
		log.Fatal().Msgf("Error publishing message: %s", err)
		return err
	}

	return nil
}
