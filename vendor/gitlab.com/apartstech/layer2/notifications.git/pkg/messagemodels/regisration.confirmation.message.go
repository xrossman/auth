package messagemodels

type RegistrationConfirmationEmailMessage struct {
	HREF      string `json:"href"`
	ShortName string `json:"short_name"`
	Code      string `json:"code"`
	To        string `json:"to"`
}

func (re RegistrationConfirmationEmailMessage) TemplateName() string {
	return "registration_confirmation"
}

func (re RegistrationConfirmationEmailMessage) TemplateType() string {
	return "email"
}

func (re RegistrationConfirmationEmailMessage) Ads() string {
	return re.Code
}

func NewRegistrationConfirmationEmail(url, serviceName, to string) RegistrationConfirmationEmailMessage {
	return RegistrationConfirmationEmailMessage{
		HREF:      url,
		ShortName: serviceName,
		To:        to,
	}
}
