package messagemodels

type TestEmailMessage struct {
	To string `json:"to"`
}

func (em TestEmailMessage) TemplateName() string {
	return "email_test"
}

func (em TestEmailMessage) TemplateType() string {
	return "email"
}

func (em TestEmailMessage) Ads() string {
	return ""
}
