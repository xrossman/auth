package models

import "strings"

func PrepareFTSQuery(q string) string {
	ss := strings.Split(q, " ")
	result := make([]string, 0)

	for _, s := range ss {
		s = strings.TrimSpace(s)
		if len(s) < 3 {
			continue
		}

		s += ":*"
		result = append(result, s)
	}

	return strings.Join(result, " | ")
}
