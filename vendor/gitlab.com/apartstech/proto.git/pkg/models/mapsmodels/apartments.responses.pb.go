// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: gitlab.com/apartstech/proto.git/proto/models/mapsmodels/apartments.responses.proto

package mapsmodels

import (
	fmt "fmt"
	proto "github.com/gogo/protobuf/proto"
	io "io"
	math "math"
	math_bits "math/bits"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.GoGoProtoPackageIsVersion3 // please upgrade the proto package

type ReadApartmentsGeoResponse struct {
	Result               map[uint64]*Geo `protobuf:"bytes,1,rep,name=Result,proto3" json:"Result,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}        `json:"-"`
	XXX_unrecognized     []byte          `json:"-"`
	XXX_sizecache        int32           `json:"-"`
}

func (m *ReadApartmentsGeoResponse) Reset()         { *m = ReadApartmentsGeoResponse{} }
func (m *ReadApartmentsGeoResponse) String() string { return proto.CompactTextString(m) }
func (*ReadApartmentsGeoResponse) ProtoMessage()    {}
func (*ReadApartmentsGeoResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_1f90df1487dffb02, []int{0}
}
func (m *ReadApartmentsGeoResponse) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *ReadApartmentsGeoResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_ReadApartmentsGeoResponse.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *ReadApartmentsGeoResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ReadApartmentsGeoResponse.Merge(m, src)
}
func (m *ReadApartmentsGeoResponse) XXX_Size() int {
	return m.Size()
}
func (m *ReadApartmentsGeoResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_ReadApartmentsGeoResponse.DiscardUnknown(m)
}

var xxx_messageInfo_ReadApartmentsGeoResponse proto.InternalMessageInfo

func (m *ReadApartmentsGeoResponse) GetResult() map[uint64]*Geo {
	if m != nil {
		return m.Result
	}
	return nil
}

type FindNearestApartmentsResponse struct {
	ApartmentsIDs        []uint64        `protobuf:"varint,1,rep,packed,name=ApartmentsIDs,proto3" json:"ApartmentsIDs,omitempty"`
	Geo                  map[uint64]*Geo `protobuf:"bytes,2,rep,name=Geo,proto3" json:"Geo,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}        `json:"-"`
	XXX_unrecognized     []byte          `json:"-"`
	XXX_sizecache        int32           `json:"-"`
}

func (m *FindNearestApartmentsResponse) Reset()         { *m = FindNearestApartmentsResponse{} }
func (m *FindNearestApartmentsResponse) String() string { return proto.CompactTextString(m) }
func (*FindNearestApartmentsResponse) ProtoMessage()    {}
func (*FindNearestApartmentsResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_1f90df1487dffb02, []int{1}
}
func (m *FindNearestApartmentsResponse) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *FindNearestApartmentsResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_FindNearestApartmentsResponse.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *FindNearestApartmentsResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_FindNearestApartmentsResponse.Merge(m, src)
}
func (m *FindNearestApartmentsResponse) XXX_Size() int {
	return m.Size()
}
func (m *FindNearestApartmentsResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_FindNearestApartmentsResponse.DiscardUnknown(m)
}

var xxx_messageInfo_FindNearestApartmentsResponse proto.InternalMessageInfo

func (m *FindNearestApartmentsResponse) GetApartmentsIDs() []uint64 {
	if m != nil {
		return m.ApartmentsIDs
	}
	return nil
}

func (m *FindNearestApartmentsResponse) GetGeo() map[uint64]*Geo {
	if m != nil {
		return m.Geo
	}
	return nil
}

func init() {
	proto.RegisterType((*ReadApartmentsGeoResponse)(nil), "mapsmodels.ReadApartmentsGeoResponse")
	proto.RegisterMapType((map[uint64]*Geo)(nil), "mapsmodels.ReadApartmentsGeoResponse.ResultEntry")
	proto.RegisterType((*FindNearestApartmentsResponse)(nil), "mapsmodels.FindNearestApartmentsResponse")
	proto.RegisterMapType((map[uint64]*Geo)(nil), "mapsmodels.FindNearestApartmentsResponse.GeoEntry")
}

func init() {
	proto.RegisterFile("gitlab.com/apartstech/proto.git/proto/models/mapsmodels/apartments.responses.proto", fileDescriptor_1f90df1487dffb02)
}

var fileDescriptor_1f90df1487dffb02 = []byte{
	// 305 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xa4, 0x51, 0xcb, 0x4a, 0xc3, 0x40,
	0x14, 0x65, 0x9a, 0x5a, 0xe4, 0x06, 0x51, 0xb2, 0x8a, 0x01, 0x43, 0x28, 0x0a, 0x59, 0x4d, 0x30,
	0x6e, 0x44, 0x37, 0x56, 0xaa, 0xa1, 0x2e, 0xba, 0x98, 0xa5, 0xbb, 0x69, 0x73, 0x89, 0xa1, 0x49,
	0x26, 0x64, 0xa6, 0x42, 0x7f, 0xcb, 0xaf, 0x70, 0xe9, 0xc2, 0x0f, 0x90, 0x7c, 0x89, 0xe4, 0x61,
	0x13, 0x11, 0x15, 0x74, 0x77, 0x38, 0x73, 0xee, 0x79, 0x30, 0xc0, 0xa2, 0x58, 0x25, 0x7c, 0x41,
	0x97, 0x22, 0xf5, 0x78, 0xce, 0x0b, 0x25, 0x15, 0x2e, 0x1f, 0xbc, 0xbc, 0x10, 0x4a, 0xd0, 0x28,
	0x56, 0x0d, 0xf2, 0x52, 0x11, 0x62, 0x22, 0xbd, 0x94, 0xe7, 0xb2, 0x85, 0xb5, 0x38, 0xc5, 0x4c,
	0x49, 0x5a, 0xa0, 0xcc, 0x45, 0x26, 0x51, 0xd2, 0x5a, 0x6c, 0x40, 0x27, 0xb3, 0x26, 0x7f, 0xf5,
	0x8f, 0x50, 0x34, 0x76, 0xe3, 0x27, 0x02, 0x87, 0x0c, 0x79, 0x38, 0xd9, 0x26, 0x06, 0x28, 0x58,
	0x9b, 0x69, 0xcc, 0x60, 0xc4, 0x50, 0xae, 0x13, 0x65, 0x12, 0x47, 0x73, 0x75, 0xff, 0x94, 0x76,
	0x26, 0xf4, 0xdb, 0x33, 0xda, 0xdc, 0xdc, 0x64, 0xaa, 0xd8, 0xb0, 0xd6, 0xc0, 0xba, 0x03, 0xbd,
	0x47, 0x1b, 0x07, 0xa0, 0xad, 0x70, 0x63, 0x12, 0x87, 0xb8, 0x43, 0x56, 0x41, 0xe3, 0x04, 0x76,
	0x1e, 0x79, 0xb2, 0x46, 0x73, 0xe0, 0x10, 0x57, 0xf7, 0xf7, 0xfb, 0x51, 0x95, 0x79, 0xf3, 0x7a,
	0x31, 0x38, 0x27, 0xe3, 0x57, 0x02, 0x47, 0xb7, 0x71, 0x16, 0xce, 0x91, 0x17, 0x28, 0x55, 0x57,
	0x62, 0x5b, 0xfc, 0x18, 0xf6, 0x3a, 0x76, 0x36, 0x95, 0x75, 0xff, 0x21, 0xfb, 0x4c, 0x1a, 0x53,
	0xd0, 0x02, 0x14, 0xe6, 0xa0, 0xde, 0xe6, 0xf7, 0x03, 0x7f, 0x74, 0xaf, 0xea, 0x34, 0xe3, 0xaa,
	0x73, 0x2b, 0x80, 0xdd, 0x0f, 0xe2, 0x5f, 0xb3, 0xae, 0xe7, 0xcf, 0xa5, 0x4d, 0x5e, 0x4a, 0x9b,
	0xbc, 0x95, 0x36, 0xb9, 0xbf, 0xfa, 0xf5, 0x73, 0x57, 0xd1, 0xd7, 0xaf, 0xbd, 0xec, 0xe0, 0x62,
	0x54, 0x6b, 0xcf, 0xde, 0x03, 0x00, 0x00, 0xff, 0xff, 0x25, 0xc4, 0x93, 0x24, 0x87, 0x02, 0x00,
	0x00,
}

func (m *ReadApartmentsGeoResponse) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *ReadApartmentsGeoResponse) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *ReadApartmentsGeoResponse) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if m.XXX_unrecognized != nil {
		i -= len(m.XXX_unrecognized)
		copy(dAtA[i:], m.XXX_unrecognized)
	}
	if len(m.Result) > 0 {
		for k := range m.Result {
			v := m.Result[k]
			baseI := i
			if v != nil {
				{
					size, err := v.MarshalToSizedBuffer(dAtA[:i])
					if err != nil {
						return 0, err
					}
					i -= size
					i = encodeVarintApartmentsResponses(dAtA, i, uint64(size))
				}
				i--
				dAtA[i] = 0x12
			}
			i = encodeVarintApartmentsResponses(dAtA, i, uint64(k))
			i--
			dAtA[i] = 0x8
			i = encodeVarintApartmentsResponses(dAtA, i, uint64(baseI-i))
			i--
			dAtA[i] = 0xa
		}
	}
	return len(dAtA) - i, nil
}

func (m *FindNearestApartmentsResponse) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *FindNearestApartmentsResponse) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *FindNearestApartmentsResponse) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if m.XXX_unrecognized != nil {
		i -= len(m.XXX_unrecognized)
		copy(dAtA[i:], m.XXX_unrecognized)
	}
	if len(m.Geo) > 0 {
		for k := range m.Geo {
			v := m.Geo[k]
			baseI := i
			if v != nil {
				{
					size, err := v.MarshalToSizedBuffer(dAtA[:i])
					if err != nil {
						return 0, err
					}
					i -= size
					i = encodeVarintApartmentsResponses(dAtA, i, uint64(size))
				}
				i--
				dAtA[i] = 0x12
			}
			i = encodeVarintApartmentsResponses(dAtA, i, uint64(k))
			i--
			dAtA[i] = 0x8
			i = encodeVarintApartmentsResponses(dAtA, i, uint64(baseI-i))
			i--
			dAtA[i] = 0x12
		}
	}
	if len(m.ApartmentsIDs) > 0 {
		dAtA4 := make([]byte, len(m.ApartmentsIDs)*10)
		var j3 int
		for _, num := range m.ApartmentsIDs {
			for num >= 1<<7 {
				dAtA4[j3] = uint8(uint64(num)&0x7f | 0x80)
				num >>= 7
				j3++
			}
			dAtA4[j3] = uint8(num)
			j3++
		}
		i -= j3
		copy(dAtA[i:], dAtA4[:j3])
		i = encodeVarintApartmentsResponses(dAtA, i, uint64(j3))
		i--
		dAtA[i] = 0xa
	}
	return len(dAtA) - i, nil
}

func encodeVarintApartmentsResponses(dAtA []byte, offset int, v uint64) int {
	offset -= sovApartmentsResponses(v)
	base := offset
	for v >= 1<<7 {
		dAtA[offset] = uint8(v&0x7f | 0x80)
		v >>= 7
		offset++
	}
	dAtA[offset] = uint8(v)
	return base
}
func (m *ReadApartmentsGeoResponse) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	if len(m.Result) > 0 {
		for k, v := range m.Result {
			_ = k
			_ = v
			l = 0
			if v != nil {
				l = v.Size()
				l += 1 + sovApartmentsResponses(uint64(l))
			}
			mapEntrySize := 1 + sovApartmentsResponses(uint64(k)) + l
			n += mapEntrySize + 1 + sovApartmentsResponses(uint64(mapEntrySize))
		}
	}
	if m.XXX_unrecognized != nil {
		n += len(m.XXX_unrecognized)
	}
	return n
}

func (m *FindNearestApartmentsResponse) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	if len(m.ApartmentsIDs) > 0 {
		l = 0
		for _, e := range m.ApartmentsIDs {
			l += sovApartmentsResponses(uint64(e))
		}
		n += 1 + sovApartmentsResponses(uint64(l)) + l
	}
	if len(m.Geo) > 0 {
		for k, v := range m.Geo {
			_ = k
			_ = v
			l = 0
			if v != nil {
				l = v.Size()
				l += 1 + sovApartmentsResponses(uint64(l))
			}
			mapEntrySize := 1 + sovApartmentsResponses(uint64(k)) + l
			n += mapEntrySize + 1 + sovApartmentsResponses(uint64(mapEntrySize))
		}
	}
	if m.XXX_unrecognized != nil {
		n += len(m.XXX_unrecognized)
	}
	return n
}

func sovApartmentsResponses(x uint64) (n int) {
	return (math_bits.Len64(x|1) + 6) / 7
}
func sozApartmentsResponses(x uint64) (n int) {
	return sovApartmentsResponses(uint64((x << 1) ^ uint64((int64(x) >> 63))))
}
func (m *ReadApartmentsGeoResponse) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowApartmentsResponses
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: ReadApartmentsGeoResponse: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: ReadApartmentsGeoResponse: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Result", wireType)
			}
			var msglen int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowApartmentsResponses
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				msglen |= int(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if msglen < 0 {
				return ErrInvalidLengthApartmentsResponses
			}
			postIndex := iNdEx + msglen
			if postIndex < 0 {
				return ErrInvalidLengthApartmentsResponses
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			if m.Result == nil {
				m.Result = make(map[uint64]*Geo)
			}
			var mapkey uint64
			var mapvalue *Geo
			for iNdEx < postIndex {
				entryPreIndex := iNdEx
				var wire uint64
				for shift := uint(0); ; shift += 7 {
					if shift >= 64 {
						return ErrIntOverflowApartmentsResponses
					}
					if iNdEx >= l {
						return io.ErrUnexpectedEOF
					}
					b := dAtA[iNdEx]
					iNdEx++
					wire |= uint64(b&0x7F) << shift
					if b < 0x80 {
						break
					}
				}
				fieldNum := int32(wire >> 3)
				if fieldNum == 1 {
					for shift := uint(0); ; shift += 7 {
						if shift >= 64 {
							return ErrIntOverflowApartmentsResponses
						}
						if iNdEx >= l {
							return io.ErrUnexpectedEOF
						}
						b := dAtA[iNdEx]
						iNdEx++
						mapkey |= uint64(b&0x7F) << shift
						if b < 0x80 {
							break
						}
					}
				} else if fieldNum == 2 {
					var mapmsglen int
					for shift := uint(0); ; shift += 7 {
						if shift >= 64 {
							return ErrIntOverflowApartmentsResponses
						}
						if iNdEx >= l {
							return io.ErrUnexpectedEOF
						}
						b := dAtA[iNdEx]
						iNdEx++
						mapmsglen |= int(b&0x7F) << shift
						if b < 0x80 {
							break
						}
					}
					if mapmsglen < 0 {
						return ErrInvalidLengthApartmentsResponses
					}
					postmsgIndex := iNdEx + mapmsglen
					if postmsgIndex < 0 {
						return ErrInvalidLengthApartmentsResponses
					}
					if postmsgIndex > l {
						return io.ErrUnexpectedEOF
					}
					mapvalue = &Geo{}
					if err := mapvalue.Unmarshal(dAtA[iNdEx:postmsgIndex]); err != nil {
						return err
					}
					iNdEx = postmsgIndex
				} else {
					iNdEx = entryPreIndex
					skippy, err := skipApartmentsResponses(dAtA[iNdEx:])
					if err != nil {
						return err
					}
					if (skippy < 0) || (iNdEx+skippy) < 0 {
						return ErrInvalidLengthApartmentsResponses
					}
					if (iNdEx + skippy) > postIndex {
						return io.ErrUnexpectedEOF
					}
					iNdEx += skippy
				}
			}
			m.Result[mapkey] = mapvalue
			iNdEx = postIndex
		default:
			iNdEx = preIndex
			skippy, err := skipApartmentsResponses(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if (skippy < 0) || (iNdEx+skippy) < 0 {
				return ErrInvalidLengthApartmentsResponses
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			m.XXX_unrecognized = append(m.XXX_unrecognized, dAtA[iNdEx:iNdEx+skippy]...)
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func (m *FindNearestApartmentsResponse) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowApartmentsResponses
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: FindNearestApartmentsResponse: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: FindNearestApartmentsResponse: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType == 0 {
				var v uint64
				for shift := uint(0); ; shift += 7 {
					if shift >= 64 {
						return ErrIntOverflowApartmentsResponses
					}
					if iNdEx >= l {
						return io.ErrUnexpectedEOF
					}
					b := dAtA[iNdEx]
					iNdEx++
					v |= uint64(b&0x7F) << shift
					if b < 0x80 {
						break
					}
				}
				m.ApartmentsIDs = append(m.ApartmentsIDs, v)
			} else if wireType == 2 {
				var packedLen int
				for shift := uint(0); ; shift += 7 {
					if shift >= 64 {
						return ErrIntOverflowApartmentsResponses
					}
					if iNdEx >= l {
						return io.ErrUnexpectedEOF
					}
					b := dAtA[iNdEx]
					iNdEx++
					packedLen |= int(b&0x7F) << shift
					if b < 0x80 {
						break
					}
				}
				if packedLen < 0 {
					return ErrInvalidLengthApartmentsResponses
				}
				postIndex := iNdEx + packedLen
				if postIndex < 0 {
					return ErrInvalidLengthApartmentsResponses
				}
				if postIndex > l {
					return io.ErrUnexpectedEOF
				}
				var elementCount int
				var count int
				for _, integer := range dAtA[iNdEx:postIndex] {
					if integer < 128 {
						count++
					}
				}
				elementCount = count
				if elementCount != 0 && len(m.ApartmentsIDs) == 0 {
					m.ApartmentsIDs = make([]uint64, 0, elementCount)
				}
				for iNdEx < postIndex {
					var v uint64
					for shift := uint(0); ; shift += 7 {
						if shift >= 64 {
							return ErrIntOverflowApartmentsResponses
						}
						if iNdEx >= l {
							return io.ErrUnexpectedEOF
						}
						b := dAtA[iNdEx]
						iNdEx++
						v |= uint64(b&0x7F) << shift
						if b < 0x80 {
							break
						}
					}
					m.ApartmentsIDs = append(m.ApartmentsIDs, v)
				}
			} else {
				return fmt.Errorf("proto: wrong wireType = %d for field ApartmentsIDs", wireType)
			}
		case 2:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Geo", wireType)
			}
			var msglen int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowApartmentsResponses
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				msglen |= int(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if msglen < 0 {
				return ErrInvalidLengthApartmentsResponses
			}
			postIndex := iNdEx + msglen
			if postIndex < 0 {
				return ErrInvalidLengthApartmentsResponses
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			if m.Geo == nil {
				m.Geo = make(map[uint64]*Geo)
			}
			var mapkey uint64
			var mapvalue *Geo
			for iNdEx < postIndex {
				entryPreIndex := iNdEx
				var wire uint64
				for shift := uint(0); ; shift += 7 {
					if shift >= 64 {
						return ErrIntOverflowApartmentsResponses
					}
					if iNdEx >= l {
						return io.ErrUnexpectedEOF
					}
					b := dAtA[iNdEx]
					iNdEx++
					wire |= uint64(b&0x7F) << shift
					if b < 0x80 {
						break
					}
				}
				fieldNum := int32(wire >> 3)
				if fieldNum == 1 {
					for shift := uint(0); ; shift += 7 {
						if shift >= 64 {
							return ErrIntOverflowApartmentsResponses
						}
						if iNdEx >= l {
							return io.ErrUnexpectedEOF
						}
						b := dAtA[iNdEx]
						iNdEx++
						mapkey |= uint64(b&0x7F) << shift
						if b < 0x80 {
							break
						}
					}
				} else if fieldNum == 2 {
					var mapmsglen int
					for shift := uint(0); ; shift += 7 {
						if shift >= 64 {
							return ErrIntOverflowApartmentsResponses
						}
						if iNdEx >= l {
							return io.ErrUnexpectedEOF
						}
						b := dAtA[iNdEx]
						iNdEx++
						mapmsglen |= int(b&0x7F) << shift
						if b < 0x80 {
							break
						}
					}
					if mapmsglen < 0 {
						return ErrInvalidLengthApartmentsResponses
					}
					postmsgIndex := iNdEx + mapmsglen
					if postmsgIndex < 0 {
						return ErrInvalidLengthApartmentsResponses
					}
					if postmsgIndex > l {
						return io.ErrUnexpectedEOF
					}
					mapvalue = &Geo{}
					if err := mapvalue.Unmarshal(dAtA[iNdEx:postmsgIndex]); err != nil {
						return err
					}
					iNdEx = postmsgIndex
				} else {
					iNdEx = entryPreIndex
					skippy, err := skipApartmentsResponses(dAtA[iNdEx:])
					if err != nil {
						return err
					}
					if (skippy < 0) || (iNdEx+skippy) < 0 {
						return ErrInvalidLengthApartmentsResponses
					}
					if (iNdEx + skippy) > postIndex {
						return io.ErrUnexpectedEOF
					}
					iNdEx += skippy
				}
			}
			m.Geo[mapkey] = mapvalue
			iNdEx = postIndex
		default:
			iNdEx = preIndex
			skippy, err := skipApartmentsResponses(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if (skippy < 0) || (iNdEx+skippy) < 0 {
				return ErrInvalidLengthApartmentsResponses
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			m.XXX_unrecognized = append(m.XXX_unrecognized, dAtA[iNdEx:iNdEx+skippy]...)
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func skipApartmentsResponses(dAtA []byte) (n int, err error) {
	l := len(dAtA)
	iNdEx := 0
	depth := 0
	for iNdEx < l {
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return 0, ErrIntOverflowApartmentsResponses
			}
			if iNdEx >= l {
				return 0, io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= (uint64(b) & 0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		wireType := int(wire & 0x7)
		switch wireType {
		case 0:
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowApartmentsResponses
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				iNdEx++
				if dAtA[iNdEx-1] < 0x80 {
					break
				}
			}
		case 1:
			iNdEx += 8
		case 2:
			var length int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowApartmentsResponses
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				length |= (int(b) & 0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if length < 0 {
				return 0, ErrInvalidLengthApartmentsResponses
			}
			iNdEx += length
		case 3:
			depth++
		case 4:
			if depth == 0 {
				return 0, ErrUnexpectedEndOfGroupApartmentsResponses
			}
			depth--
		case 5:
			iNdEx += 4
		default:
			return 0, fmt.Errorf("proto: illegal wireType %d", wireType)
		}
		if iNdEx < 0 {
			return 0, ErrInvalidLengthApartmentsResponses
		}
		if depth == 0 {
			return iNdEx, nil
		}
	}
	return 0, io.ErrUnexpectedEOF
}

var (
	ErrInvalidLengthApartmentsResponses        = fmt.Errorf("proto: negative length found during unmarshaling")
	ErrIntOverflowApartmentsResponses          = fmt.Errorf("proto: integer overflow")
	ErrUnexpectedEndOfGroupApartmentsResponses = fmt.Errorf("proto: unexpected end of group")
)
