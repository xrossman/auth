package mapsmodels

import (
	"encoding/json"

	"github.com/google/uuid"
	"gitlab.com/apartstech/proto.git/custom"
	"googlemaps.github.io/maps"
)

func NewPlaceDetailsResponseMessage(result maps.PlaceDetailsResult) (*PlaceDetailsResponseMessage, error) {
	data, err := json.Marshal(result)
	if err != nil {
		return nil, err
	}

	return &PlaceDetailsResponseMessage{Result: data}, nil
}

func FromPBToHumanPlaceDetailsResult(message *PlaceDetailsResponseMessage) (*PlaceDetailsResponse, error) {
	result := maps.PlaceDetailsResult{}

	if err := json.Unmarshal(message.Result, &result); err != nil {
		return nil, err
	}

	return &PlaceDetailsResponse{Result: result}, nil
}

func NewGeocodingResponseMessage(result []maps.GeocodingResult) (*GeocodingResponseMessage, error) {
	data, err := json.Marshal(result)
	if err != nil {
		return nil, err
	}

	return &GeocodingResponseMessage{
		Result: data,
	}, nil
}

func FromPBToHumanGeocodingResult(message *GeocodingResponseMessage) (*GeocodingResultResponse, error) {
	results := make([]maps.GeocodingResult, 0)

	if err := json.Unmarshal(message.Result, &results); err != nil {
		return nil, err
	}

	return &GeocodingResultResponse{Result: results}, nil
}

func NewPlaceSearchResponseMessage(response maps.FindPlaceFromTextResponse) (*SearchResponse, error) {
	data, err := custom.NewPlaceSearchFeatures(response)
	if err != nil {
		return nil, err
	}
	return &SearchResponse{
		Features: data,
	}, nil
}

func FromPBToHumanPlaceSearchResponse(response *SearchResponse) (*PlaceSearchResponse, error) {
	features := make([]maps.PlacesSearchResult, 0)
	if err := json.Unmarshal(response.GetFeatures(), &features); err != nil {
		return nil, err
	}

	return &PlaceSearchResponse{Features: features}, nil
}

func NewAutocompleteResponse(data maps.AutocompleteResponse, sessionToken maps.PlaceAutocompleteSessionToken) (*PlacesAutocompleteResponseMessage, error) {
	result, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}

	return &PlacesAutocompleteResponseMessage{
		Result:       result,
		SessionToken: uuid.UUID(sessionToken).String(),
	}, nil
}

func FromPBToHumanPlacesAutoCompleteResponse(response *PlacesAutocompleteResponseMessage) (*PlacesAutocompleteResponse, error) {
	predictions := maps.AutocompleteResponse{}
	if err := json.Unmarshal(response.Result, &predictions); err != nil {
		return nil, err
	}

	return &PlacesAutocompleteResponse{Result: predictions, SessionToken: response.SessionToken}, nil
}
