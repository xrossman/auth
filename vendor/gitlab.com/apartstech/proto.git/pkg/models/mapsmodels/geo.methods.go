package mapsmodels

import (
	"github.com/twpayne/go-geom"
	"github.com/twpayne/go-geom/encoding/ewkb"
	"github.com/twpayne/go-geom/encoding/ewkbhex"
)

func (m *Geo) Equal(g *Geo) bool {
	if m == nil && g == nil {
		return true
	}

	if (m != nil && g == nil) || (m == nil && g != nil) {
		return false
	}

	return m.FormattedAddress == g.FormattedAddress &&
		m.PlaceId == g.PlaceId &&
		m.Lat == g.Lat &&
		m.Lng == g.Lng
}

func (m *Geo) GetPGISValue() (*ewkb.Point, error) {
	p, err := geom.NewPoint(geom.XY).SetCoords([]float64{m.Lng, m.Lat})
	if err != nil {
		return nil, err
	}
	w := &ewkb.Point{Point: p.SetSRID(4326)}
	return w, nil
}

func (m *Geo) SetLatLon(s string) error {
	t, err := ewkbhex.Decode(s)
	if err != nil {
		return err
	}

	flatCoords := t.FlatCoords()
	if len(t.FlatCoords()) == 2 {
		m.Lng = flatCoords[0]
		m.Lat = flatCoords[1]
	}

	return nil
}
