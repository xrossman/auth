package mapsmodels

import "googlemaps.github.io/maps"

type PlaceSearchResponse struct {
	Features []maps.PlacesSearchResult `json:"features"`
}

type GeocodingResultResponse struct {
	Result []maps.GeocodingResult `json:"result"`
}

type PlaceDetailsResponse struct {
	Result maps.PlaceDetailsResult `json:"result"`
}

type PlacesAutocompleteResponse struct {
	Result       maps.AutocompleteResponse `json:"result"`
	SessionToken string                    `json:"session_token"`
}
