package apartmentsmodels

import (
	"bytes"
	"fmt"
	"strconv"
	"time"

	sq "github.com/Masterminds/squirrel"
	"github.com/gin-gonic/gin"
	"github.com/gogo/protobuf/jsonpb"
	"gitlab.com/apartstech/proto.git/custom"
	"gitlab.com/apartstech/proto.git/pkg/models"
	"gitlab.com/apartstech/proto.git/pkg/models/aclmodels"
	"gitlab.com/apartstech/proto.git/pkg/models/mapsmodels"
)

func NewApartmentStatusFromString(status string, expiresAt *time.Time) (*ApartmentStatusObject, error) {
	var (
		stValue int32
		ok      bool
	)
	if stValue, ok = ApartmentStatus_value[status]; !ok {
		return nil, fmt.Errorf("unknown status value `%s`", status)
	}

	now := time.Now()
	st := ApartmentStatus(stValue)
	return &ApartmentStatusObject{
		Status:     &st,
		AcceptedAt: &now,
		ExpiresAt:  expiresAt,
	}, nil
}

func (m *ApartmentStatusObject) Publish() {
	if m.Status != nil {
		if *m.Status == ApartmentStatus_published {
			return
		}
	}

	now := time.Now()
	m.AcceptedAt = &now
	st := ApartmentStatus_published
	m.Status = &st
}

func (m *ApartmentStatusObject) Template() {
	if m.Status != nil {
		if *m.Status == ApartmentStatus_template {
			return
		}
	}

	now := time.Now()
	m.AcceptedAt = &now
	st := ApartmentStatus_template
	m.Status = &st
}

func (m *ApartmentStatusObject) Archive() {
	if m.Status != nil {
		if *m.Status == ApartmentStatus_archived {
			return
		}
	}

	now := time.Now()
	m.AcceptedAt = &now
	st := ApartmentStatus_archived
	m.Status = &st
}

func (m *ApartmentStatusObject) Blocked() {
	if m.Status != nil {
		if *m.Status == ApartmentStatus_blocked {
			return
		}
	}

	now := time.Now()
	m.AcceptedAt = &now
	st := ApartmentStatus_blocked
	m.Status = &st
}

func (m *Apartment) GetCustomAmenities() []custom.KeyValuer {
	if len(m.Amenities) == 0 {
		return nil
	}

	amenities := make([]custom.KeyValuer, 0)
	for _, a := range m.Amenities {
		amenities = append(amenities, a)
	}

	return amenities
}

func (m ApartmentsFilter) GetDBScope(b sq.SelectBuilder, noOrder bool) sq.SelectBuilder {
	if len(m.IDs) != 0 {
		b = b.Where(sq.Eq{"a.id": m.IDs})
	}

	if len(m.ExceptIDs) != 0 {
		b = b.Where(sq.NotEq{"a.id": m.ExceptIDs})
	}

	//if m.NumberOfRooms != nil {
	//	b = b.LeftJoin(
	//		"apartments.rooms on apartments.rooms.id = apartments.apartments.id",
	//	).
	//		GroupBy("apartments.apartments.id").
	//		Having(sq.Eq{"COUNT(apartments.rooms.id)": *m.NumberOfRooms})
	//}

	if m.MinCosts != nil {
		b = b.Where(sq.GtOrEq{"ap.amount": *m.MinCosts})
	}

	if m.MaxCosts != nil {
		b = b.Where(sq.LtOrEq{"ap.amount": *m.MaxCosts})
	}

	if len(m.RealtorsIDS) != 0 {
		b = b.Where(sq.Eq{"a.realtor_id": m.RealtorsIDS})
	}

	if m.MaxNumberOfGuests != nil {
		b = b.Where(sq.GtOrEq{"a.max_number_of_guests": *m.MaxNumberOfGuests})
	}

	if m.RentPeriod != nil {
		b = b.Where(sq.Eq{"a.rent_period": m.RentPeriod.String()})
	}

	if m.AmenitiesIDs != nil {
		if len(*m.AmenitiesIDs) != 0 {
			b = m.AmenitiesIDs.GetDBScope(b)
		}
	}

	if rank := m.GetRanks(); rank != "" {
		b = b.Where(sq.Eq{"a.rank": rank})
	}

	if atype := m.GetTypes(); atype != "" {
		b = b.Where(sq.Eq{"a.type": atype})
	}

	if m.Statuses != nil {
		b = b.Where(sq.Eq{"a.status ->> 'status'": m.GetStatuses()})
		if !noOrder {
			b = b.OrderBy("a.status ->> 'status', a.id")
		}
	}

	if m.ExcludeStatuses != nil {
		b = b.Where(sq.NotEq{"a.status ->> 'status'": m.GetExcludeStatuses()})
	}

	if q := m.GetQuery(); q != "" {
		b = b.Where(fmt.Sprintf("a.searchable_text @@ to_tsquery('%s')", models.PrepareFTSQuery(q)))
		if !noOrder {
			b = b.OrderBy(fmt.Sprintf("ts_rank(a.searchable_text, to_tsquery('%s')) DESC", models.PrepareFTSQuery(q)))
		}
	}

	if !noOrder {
		b = b.OrderBy("id")
	}

	return b
}

func (m *ApartmentStatusObject) MarshalJSON() ([]byte, error) {
	b := bytes.Buffer{}

	mr := jsonpb.Marshaler{
		OrigName:     true,
		EmitDefaults: true,
	}

	err := mr.Marshal(&b, m)
	if err != nil {
		return nil, err
	}

	return b.Bytes(), nil
}

func (m *ApartmentStatusObject) UnmarshalJSON(d []byte) error {
	mr := jsonpb.Unmarshaler{}

	r := bytes.NewReader(d)
	return mr.Unmarshal(r, m)
}

func (m *ApartmentStatusObject) Scan(v interface{}) error {
	switch t := v.(type) {
	case []byte:
		return m.UnmarshalJSON(t)
	}

	return fmt.Errorf("unexpected value type: %T", v)
}

func NewApartmentsFilterFromContext(c *gin.Context) (*ApartmentsFilter, error) {
	var f ApartmentsFilter
	rp := c.Query("rp")

	if err := c.BindQuery(&f); err != nil {
		return nil, err
	}

	if rp != "" {
		if v, ok := RentPeriod_value[rp]; ok {
			value := RentPeriod(v)
			f.RentPeriod = &value
		}
	}

	if f.Polygon == nil {
		radiusRate, geo, err := getGeoFromContext(c)
		if err != nil {
			return nil, err
		}

		f.Geo = geo
		f.RadiusRate = &radiusRate
	}

	return &f, nil
}

func getGeoFromContext(c *gin.Context) (int64, *mapsmodels.Geo, error) {
	latVal := c.Query("lat")
	lonVal := c.Query("lng")

	if latVal == "" || lonVal == "" {
		return 0, nil, nil
	}

	var (
		err        error
		lat, lon   float64
		radiusRate int
	)

	g := new(mapsmodels.Geo)
	lat, err = strconv.ParseFloat(latVal, 64)
	if err != nil {
		return 0, nil, err
	}

	lon, err = strconv.ParseFloat(lonVal, 64)
	if err != nil {
		return 0, nil, err
	}

	g.Lat = lat
	g.Lng = lon

	radiusRateVal := c.Query("r")
	if radiusRateVal == "" {
		return 1000, g, nil
	}

	radiusRate, err = strconv.Atoi(radiusRateVal)
	if err != nil {
		return 0, nil, err
	}

	return int64(radiusRate), g, nil
}

func NewApartmentsResponse(a Apartment, u aclmodels.User, amenities []custom.KeyValuer) *ApartmentResponse {
	a.Amenities = nil
	return &ApartmentResponse{
		Apartment:    &a,
		Realtor:      &u,
		AmenitiesMap: custom.NewMapStringSliceOffStrings(amenities),
	}
}

func NewListApartmentRequest(p *models.OffsetLimitPagination, f *ApartmentsFilter) *ListApartmentsRequest {
	return &ListApartmentsRequest{
		Pagination: p,
		Filter:     f,
	}
}

func NewListApartmentsResponse(
	apartments []Apartment,
	realtors map[string]aclmodels.User,
	counts map[string]uint64,
	geo map[uint64]*mapsmodels.Geo,
	p *models.OffsetLimitPagination,
) (*ListApartmentsResponse, error) {
	data := make([]*ApartmentResponse, 0)
	for _, a := range apartments {
		var (
			realtor aclmodels.User
			ok      bool
		)
		if realtor, ok = realtors[a.RealtorID]; !ok {
			return nil, fmt.Errorf("can't find realtor for apartment %d", a.ID)
		}

		if v, ok := counts[realtor.UUID]; ok {
			realtor.ApartmentsCount = v
		}

		if v, ok := geo[a.ID]; ok {
			a.Geo = v
		}

		r := NewApartmentsResponse(a, realtor, a.GetCustomAmenities())
		data = append(data, r)
	}

	return &ListApartmentsResponse{
		Apartments: data,
		Pagination: p,
	}, nil
}
