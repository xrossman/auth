package apartmentsmodels

func (m Amenity) Key() string {
	return m.Category
}

func (m Amenity) Value() string {
	return m.Type
}
