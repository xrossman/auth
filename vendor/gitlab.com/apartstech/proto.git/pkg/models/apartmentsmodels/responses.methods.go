package apartmentsmodels

func NewListApartmentRoomsResponse(rooms []Room) *ListApartmentRoomsResponse {
	r := make([]*Room, 0)

	for _, room := range rooms {
		r = append(r, &room)
	}

	return &ListApartmentRoomsResponse{
		Rooms: r,
	}
}

func NewListGroupedAmenitiesResponse(amenities []*Amenity) *ListGroupedAmenities {
	res := make(map[string]*RepeatedAmenities)

	for _, a := range amenities {
		if _, ok := res[a.Category]; !ok {
			res[a.Category] = &RepeatedAmenities{
				List: make([]*Amenity, 0),
			}
		}
		l := res[a.Category]
		l.List = append(res[a.Category].List, a)
		res[a.Category] = l
	}

	return &ListGroupedAmenities{
		Amenities: res,
	}
}
