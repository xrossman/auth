package apartmentsmodels

func (m *Room) Beds() uint64 {
	return uint64(m.QueenSizeBed +
		m.KingSizeBed + m.TwoPersonBed +
		m.OnePersonBed + m.TwoPersonSofa +
		m.OnePersonSofa)
}
