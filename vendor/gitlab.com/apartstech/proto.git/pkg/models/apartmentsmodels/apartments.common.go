package apartmentsmodels

const (
	USDCurrency = "USD"
	MXNCurrency = "MXN"
)

var AvailableCurrencies = []string{
	USDCurrency,
	MXNCurrency,
}

func GetMissingCurrencies(prices []ApartmentPrice) []string {
	result := make([]string, 0)
	for _, c := range AvailableCurrencies {
		exists := false
		for _, p := range prices {
			if c == p.GetCurrency() {
				exists = true
				break
			}

		}

		if !exists {
			result = append(result, c)
		}
	}

	if len(result) > 0 {
		return result
	}

	return nil
}
