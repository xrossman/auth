package models

import (
	"math"
	"strconv"

	sq "github.com/Masterminds/squirrel"
	"github.com/gin-gonic/gin"
)

const (
	pageQueryParam    = "page"
	perPageQueryParam = "per_page"

	defaultLimitValue = uint64(20)
)

func (m *Pagination) IncrementPage() bool {
	m.Page++
	return m.Page <= m.Pages
}

func (m *Pagination) GetLimit() uint64 {
	return uint64(m.PerPage * m.Page)
}

func (m *Pagination) GetOffset() uint64 {
	if m.Page == 1 {
		return 0
	}

	return uint64(m.PerPage * (m.Page - 1))
}

func (m *Pagination) CountPages() {
	if m.PerPage == 0 || m.Total == 0 {
		m.Pages = 0
		return
	}

	if m.Page == 0 {
		m.Page = 1
	}

	m.Pages = int64(math.Ceil(float64(m.Total) / float64(m.PerPage)))
}

func (m *Pagination) QueryParams() map[string]string {
	params := make(map[string]string)
	if m.Page > 0 {
		params[pageQueryParam] = strconv.FormatInt(m.Page, 10)
	}
	if m.PerPage > 0 {
		params[perPageQueryParam] = strconv.FormatInt(m.PerPage, 10)
	}
	if m.Total > 0 {
		params["total"] = strconv.FormatInt(m.Total, 10)
	}
	if m.Pages > 0 {
		params["pages"] = strconv.FormatInt(m.Pages, 10)
	}
	return params
}

func (m *OffsetLimitPagination) QueryBuilder(b sq.SelectBuilder) sq.SelectBuilder {
	b = b.Offset(m.GetOffset())
	limit := m.GetLimit()

	if limit == 0 {
		limit = defaultLimitValue
	}

	return b.Limit(limit)
}

func NewOffsetLimitPaginationFromGinContext(c *gin.Context) (*OffsetLimitPagination, error) {
	p := OffsetLimitPagination{}
	if err := c.ShouldBindQuery(&p); err != nil {
		return nil, err
	}

	if p.GetLimit() == 0 {
		p.Limit = defaultLimitValue
	}

	return &p, nil
}

func NewPaginationFromGinContext(c *gin.Context) (*Pagination, error) {
	p := Pagination{}
	if err := c.ShouldBindQuery(&p); err != nil {
		return nil, err
	}

	if p.GetLimit() == 0 {
		return nil, nil
	}

	return &p, nil
}
