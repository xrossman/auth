package models

func NewStatusResponse(s bool) *StatusResponse {
	return &StatusResponse{
		Status: s,
	}
}

func NewUintIDResponse(id uint64) *UintIDResponse {
	return &UintIDResponse{
		ID: id,
	}
}
