package helpers

import (
	"context"
)

const defaultLang = "en"

func GetLangFromContext(ctx context.Context) (string, error) {
	var (
		lang string
		ok   bool
	)

	langValue := ctx.Value("lang")

	lang, ok = langValue.(string)
	if !ok || lang == "" {
		lang = defaultLang
	}

	return lang, nil
}
