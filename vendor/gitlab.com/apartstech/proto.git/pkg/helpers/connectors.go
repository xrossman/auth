package helpers

import (
	"fmt"
	"log"
	"net"
	"time"

	"gitlab.com/apartstech/proto.git/pkg/config"
	"google.golang.org/grpc"
	"google.golang.org/grpc/connectivity"
	"google.golang.org/grpc/credentials"
)

func GetGRPCConn(cfg config.GRPCConfig) (*grpc.Server, net.Listener, error) {
	lis, err := net.Listen("tcp", cfg.Target())
	if err != nil {
		return nil, nil, fmt.Errorf("failed to listen: %v", err)
	}

	var opts []grpc.ServerOption
	if !cfg.Insecure {
		creds, err := credentials.NewServerTLSFromFile(cfg.CertFilePath, cfg.KeyFilePath)
		if err != nil {
			return nil, nil, fmt.Errorf("could not load TLS keys: %pingHandlers", err)
		}

		opts = []grpc.ServerOption{
			grpc.Creds(creds),
		}
	}

	s := grpc.NewServer(opts...)

	return s, lis, nil
}

func GetGRPClientConn(cfg config.GRPCConfig) (*grpc.ClientConn, error) {
	var (
		creds credentials.TransportCredentials
		err   error
	)

	opts := make([]grpc.DialOption, 0)

	if !cfg.Insecure {
		creds, err = credentials.NewClientTLSFromFile(cfg.CertFilePath, "")
		if err != nil {
			return nil, err
		}
		opts = []grpc.DialOption{
			grpc.WithTransportCredentials(creds),
		}
	}

	if cfg.Insecure {
		opts = append(opts, grpc.WithInsecure())
	}

	conn, err := grpc.Dial(cfg.Target(), opts...)
	if err != nil {
		return nil, err
	}

	return conn, nil
}

//Run it only in routine!!!
func CheckConnectionState(conn *grpc.ClientConn, cfg config.GRPCConfig) {
	if cfg.ConnectionCheckingDelay.Seconds() == 0 {
		return
	}

	var err error

	for {
		time.Sleep(cfg.ConnectionCheckingDelay)
		if conn.GetState() != connectivity.Ready {
			log.Printf("[%s]: current connection state is %s. Trying to reconnect.", cfg.Target(), conn.GetState())

			conn, err = GetGRPClientConn(cfg)
			if err != nil {
				log.Printf("can't connect to %s. Error ocured: %s\nWaiting for %s...\n", cfg.Target(), err, cfg.ConnectionCheckingDelay)
				continue
			}

			if conn.GetState() != connectivity.Ready {
				log.Printf("failed to connect to %s. Trying to reconnect.", cfg.Target())
				continue
			}
		}
	}
}
