package config

import (
	"fmt"
	"time"
)

type GRPCConfig struct {
	Host                    string        `yaml:"host"`
	Port                    uint          `yaml:"port"`
	CertFilePath            string        `yaml:"cert_file_path"`
	KeyFilePath             string        `yaml:"key_file_path"`
	ConnectionCheckingDelay time.Duration `yaml:"connection_checking_delay"`
	Insecure                bool          `yaml:"insecure"`
	Enabled                 bool          `yaml:"enabled"`
}

func (c GRPCConfig) Target() string {
	return fmt.Sprintf("%s:%d", c.Host, c.Port)
}
