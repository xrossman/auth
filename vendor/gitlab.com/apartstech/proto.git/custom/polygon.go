package custom

import (
	"reflect"
	"strconv"

	"github.com/twpayne/go-geom"
	"github.com/ugorji/go/codec"
)

type Polygon [][]float64

func (m *Polygon) MarshalTo(data []byte) (int, error) {
	var (
		mh codec.MsgpackHandle
		b  []byte
	)

	mh.MapType = reflect.TypeOf(map[string][]string(nil))
	enc := codec.NewEncoderBytes(&b, &mh)

	if err := enc.Encode(m); err != nil {
		return 0, err
	}

	copy(data, b)
	return len(b), nil
}

func (m *Polygon) Unmarshal(data []byte) error {
	var (
		mh codec.MsgpackHandle
	)

	dec := codec.NewDecoderBytes(data, &mh)

	return dec.Decode(&m)
}

func (m *Polygon) Size() int {
	b, err := m.Marshal()
	if err != nil {
		return 0
	}
	return len(b)
}

func (m *Polygon) Marshal() ([]byte, error) {
	var (
		mh codec.MsgpackHandle
		b  []byte
	)

	enc := codec.NewEncoderBytes(&b, &mh)

	err := enc.Encode(m)

	return b, err
}

func (m *Polygon) GetCoords() [][]geom.Coord {
	cc := make([][]geom.Coord, 1)
	for _, p := range *m {
		c := geom.Coord(p)
		cc[0] = append(cc[0], c)
	}

	return cc
}

func NewPolygonFromString(v string) (*Polygon, error) {
	result := make([][]float64, 0)
	point := make([]float64, 2)
	xs, ys := make([]int32, 0), make([]int32, 0)

	isX := true
	for i, s := range v {
		if s == ',' {
			x, err := strconv.ParseFloat(string(xs), 64)
			if err != nil {
				return nil, err
			}

			xs = make([]int32, 0)
			point[0] = x
			isX = false
			continue
		}

		if s == ':' {
			y, err := strconv.ParseFloat(string(ys), 64)
			if err != nil {
				return nil, err
			}

			ys = make([]int32, 0)
			point[1] = y
			result = append(result, point)
			point = make([]float64, 2)
			isX = true
			continue
		}

		if i == len(v)-1 {
			ys = append(ys, s)
			y, err := strconv.ParseFloat(string(ys), 64)
			if err != nil {
				return nil, err
			}

			point[1] = y

			result = append(result, point)
			continue
		}

		if isX {
			xs = append(xs, s)
			continue
		}

		ys = append(ys, s)
	}

	p := Polygon(result)

	return &p, nil
}
