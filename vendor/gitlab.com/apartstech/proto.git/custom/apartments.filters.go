package custom

import (
	sq "github.com/Masterminds/squirrel"
	"github.com/ugorji/go/codec"
)

type Filterer interface {
	GetDBScope(db sq.SelectBuilder) sq.SelectBuilder
}

type AmenitiesByIDSFilter []uint64

type AmenitiesByTypesFilter []string

type AmenitiesByCategoryFilter []string

func (fn *AmenitiesByTypesFilter) GetDBScope(b sq.SelectBuilder) sq.SelectBuilder {
	sq.Select()
	if len(*fn) != 0 {
		b = b.LeftJoin(
			"apartments.amenities_to_apartments as ata on ata.apartment_id = a.id",
		).LeftJoin(
			"apartments.amenities on ata.amenity_id = apartments.amenities.id and a.type in (?)", fn,
		)
	}

	return b
}

func (fid *AmenitiesByIDSFilter) GetDBScope(b sq.SelectBuilder) sq.SelectBuilder {
	if len(*fid) != 0 {
		b = b.LeftJoin(
			"apartments.amenities_to_apartments as ata on ata.apartment_id = apartments.apartments.id and ata.apartment_id in (?)", fid,
		)
	}

	return b
}

func (fid *AmenitiesByIDSFilter) MarshalTo(data []byte) (int, error) {
	b, err := fid.Marshal()
	if err != nil {
		return 0, err
	}

	copy(data, b)

	return len(b), nil
}

func (fid *AmenitiesByIDSFilter) Marshal() ([]byte, error) {
	var (
		mh codec.MsgpackHandle
		b  []byte
	)

	enc := codec.NewEncoderBytes(&b, &mh)

	err := enc.Encode(fid)
	return b, err
}

func (fid *AmenitiesByIDSFilter) Unmarshal(data []byte) error {
	var (
		mh codec.MsgpackHandle
	)

	dec := codec.NewDecoderBytes(data, &mh)

	return dec.Decode(&fid)
}

func (fid *AmenitiesByIDSFilter) Size() int {
	b, _ := fid.Marshal()
	return len(b)
}

func (fn *AmenitiesByTypesFilter) MarshalTo(data []byte) (int, error) {
	b, err := fn.Marshal()
	if err != nil {
		return 0, err
	}

	copy(data, b)

	return len(b), nil
}

func (fn *AmenitiesByTypesFilter) Unmarshal(data []byte) error {
	var (
		mh codec.MsgpackHandle
	)

	dec := codec.NewDecoderBytes(data, &mh)

	return dec.Decode(&fn)
}

func (fn *AmenitiesByTypesFilter) Size() int {
	b, _ := fn.Marshal()
	return len(b)
}

func (fn *AmenitiesByTypesFilter) Marshal() ([]byte, error) {
	var (
		mh codec.MsgpackHandle
		b  []byte
	)

	enc := codec.NewEncoderBytes(&b, &mh)
	err := enc.Encode(fn)

	return b, err
}
