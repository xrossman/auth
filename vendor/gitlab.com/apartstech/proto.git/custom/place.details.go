package custom

import (
	"encoding/json"
	"fmt"

	"googlemaps.github.io/maps"
)

type PlaceDetails struct {
	maps.PlaceDetailsResult
	len int
}

func (pd *PlaceDetails) Unmarshal(data []byte) error {
	return json.Unmarshal(data, &pd)
}

func (pd *PlaceDetails) Size() int {
	b, err := json.Marshal(pd)
	if err != nil {
		return 0
	}

	pd.len = len(b)
	return pd.len
}

func (pd *PlaceDetails) MarshalTo(data []byte) (n int, err error) {
	fmt.Printf("%s", data)
	if pd.Size() == 0 {
		return 0, nil
	}

	b, err := json.Marshal(pd)
	if err != nil {
		return 0, err
	}

	copy(data, b)

	return 0, nil
}
