package custom

import (
	"encoding/json"
	"fmt"

	"googlemaps.github.io/maps"
)

type PlaceSearchResult struct {
	maps.PlacesSearchResult
	len int
}

func (psr *PlaceSearchResult) Unmarshal(data []byte) error {
	return json.Unmarshal(data, &psr)
}

func (psr *PlaceSearchResult) Size() int {
	b, err := json.Marshal(psr)
	if err != nil {
		return 0
	}

	psr.len = len(b)
	return psr.len
}

func (psr *PlaceSearchResult) MarshalTo(data []byte) (n int, err error) {
	fmt.Printf("%s", data)
	if psr.Size() == 0 {
		return 0, nil
	}

	b, err := json.Marshal(psr)
	if err != nil {
		return 0, err
	}

	copy(data, b)

	return 0, nil
}

type PlacesFeatures struct {
	Candidates       []PlaceSearchResult `json:"candidates"`
	HTMLAttributions []string            `json:"html_attributions"`
	len              int
}

func (f *PlacesFeatures) Unmarshal(data []byte) error {
	return json.Unmarshal(data, &f)
}

func (f *PlacesFeatures) Size() int {
	b, err := json.Marshal(f)
	if err != nil {
		return 0
	}

	f.len = len(b)
	return f.len
}

func (f *PlacesFeatures) MarshalTo(data []byte) (n int, err error) {
	fmt.Printf("%s", data)
	if f.Size() == 0 {
		return 0, nil
	}

	b, err := json.Marshal(f)
	if err != nil {
		return 0, err
	}

	copy(data, b)

	return 0, nil
}

func NewPlaceSearchResult(result maps.PlacesSearchResult) PlaceSearchResult {
	return PlaceSearchResult{
		PlacesSearchResult: result,
	}
}

func NewPlaceSearchFeatures(response maps.FindPlaceFromTextResponse) ([]byte, error) {
	return json.Marshal(response.Candidates)
}
