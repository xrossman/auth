package custom

type KeyValuer interface {
	Key() string
	Value() string
}
