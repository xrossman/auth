package custom

import guid "github.com/google/uuid"

type UUID struct {
	guid.UUID
}

// MarshalTo exists to fit gogoprotobuf custom type interface.
// MarshalBinary implements encoding.BinaryMarshaler.
// MarshalTo exists to fit gogoprotobuf custom type interface.
func (u UUID) MarshalTo(data []byte) (n int, err error) {
	if u.UUID == guid.Nil {
		return 0, nil
	}

	b, err := u.MarshalBinary()
	copy(data, b)

	return len(b), err
}

func (u UUID) MarshalBinary() ([]byte, error) {
	return u.UUID[:], nil
}

// Unmarshal exists to fit gogoprotobuf custom type interface.
func (u *UUID) Unmarshal(data []byte) error {
	if len(data) == 0 {
		u = nil
		return nil
	}

	return u.UUID.UnmarshalBinary(data)
}

// Size exists to fit gogoprotobuf custom type interface.
func (u *UUID) Size() int {
	if u == nil {
		return 0
	}
	return len(u.UUID)
}

func (u *UUID) Equal(other UUID) bool {
	return u.String() == other.String()
}

func ParseCustomUUID(s string) (*UUID, error) {
	u, err := guid.Parse(s)
	if err != nil {
		return nil, err
	}

	return &UUID{u}, nil
}

func SetCustomUUID(uuid guid.UUID) UUID {
	return UUID{uuid}
}

func SetCustomListUUID(uids ...guid.UUID) []UUID {
	list := make([]UUID, 0, len(uids))
	for _, v := range uids {
		list = append(list, UUID{v})
	}

	return list
}
