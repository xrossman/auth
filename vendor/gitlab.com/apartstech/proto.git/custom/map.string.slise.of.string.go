package custom

import (
	"reflect"

	"github.com/ugorji/go/codec"
)

type MapStringSliceOffStrings map[string][]string

func (m *MapStringSliceOffStrings) MarshalTo(data []byte) (int, error) {
	var (
		mh codec.MsgpackHandle
		b  []byte
	)

	mh.MapType = reflect.TypeOf(map[string][]string(nil))
	enc := codec.NewEncoderBytes(&b, &mh)

	if err := enc.Encode(m); err != nil {
		return 0, err
	}

	copy(data, b)
	return len(b), nil
}

func (m *MapStringSliceOffStrings) Unmarshal(data []byte) error {
	var (
		mh codec.MsgpackHandle
	)

	dec := codec.NewDecoderBytes(data, &mh)

	return dec.Decode(&m)
}

func (m *MapStringSliceOffStrings) Size() int {
	b, err := m.Marshal()
	if err != nil {
		return 0
	}
	return len(b)
}

func (m *MapStringSliceOffStrings) Marshal() ([]byte, error) {
	var (
		mh codec.MsgpackHandle
		b  []byte
	)

	enc := codec.NewEncoderBytes(&b, &mh)

	err := enc.Encode(m)

	return b, err
}

func NewMapStringSliceOffStrings(a []KeyValuer) MapStringSliceOffStrings {
	m := make(map[string][]string)

	for _, v := range a {
		if _, ok := m[v.Key()]; !ok {
			m[v.Key()] = make([]string, 0)
		}

		m[v.Key()] = append(m[v.Key()], v.Value())
	}

	result := MapStringSliceOffStrings(m)
	return result
}
