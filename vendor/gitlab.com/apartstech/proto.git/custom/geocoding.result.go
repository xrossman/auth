package custom

import (
	"encoding/json"
	"fmt"

	"googlemaps.github.io/maps"
)

type GeocodingResult struct {
	maps.GeocodingResult
	len int
}

func (gr *GeocodingResult) Unmarshal(data []byte) error {
	return json.Unmarshal(data, &gr.GeocodingResult)
}

func (gr *GeocodingResult) Size() int {
	b, err := json.Marshal(gr.GeocodingResult)
	if err != nil {
		return 0
	}

	gr.len = len(b)
	return gr.len
}

func (gr *GeocodingResult) MarshalTo(data []byte) (n int, err error) {
	fmt.Printf("%s", data)
	if gr.Size() == 0 {
		return 0, nil
	}

	b, err := json.Marshal(gr.GeocodingResult)
	if err != nil {
		return 0, err
	}

	copy(data, b)

	return 0, nil
}
