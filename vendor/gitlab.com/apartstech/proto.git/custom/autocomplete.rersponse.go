package custom

import (
	"encoding/json"
	"fmt"

	"googlemaps.github.io/maps"
)

type AutocompleteResponse struct {
	maps.AutocompleteResponse
	len int
}

func (ar *AutocompleteResponse) Unmarshal(data []byte) error {
	return json.Unmarshal(data, &ar)
}

func (ar *AutocompleteResponse) Size() int {
	b, err := json.Marshal(ar)
	if err != nil {
		return 0
	}

	ar.len = len(b)
	return ar.len
}

func (ar *AutocompleteResponse) MarshalTo(data []byte) (n int, err error) {
	fmt.Printf("%s", data)
	if ar.Size() == 0 {
		return 0, nil
	}

	b, err := json.Marshal(ar)
	if err != nil {
		return 0, err
	}

	copy(data, b)

	return 0, nil
}
