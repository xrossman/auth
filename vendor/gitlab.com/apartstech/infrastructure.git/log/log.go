// Package log provides a global logger for zerolog.
package log

import (
	"context"
	"fmt"
	"io"
	stdlog "log"
	"os"
	"strconv"
	"time"

	"github.com/google/uuid"
	"github.com/microparts/logs-go"
	"github.com/rs/zerolog"

	"gitlab.com/apartstech/infrastructure.git/constant"
)

// logger is the global logger.
var logger = defaultLogger()

type LogCfg struct {
	Type   string             `yaml:"type"`
	Level  string             `yaml:"level"`
	Format string             `yaml:"format"`
	Sentry *logs.SentryConfig `yaml:"sentry,omitempty"`
}

func Logger() zerolog.Logger {
	return logger
}

// Output duplicates the global logger and sets w as its output.
func Output(w io.Writer) zerolog.Logger {
	return logger.Output(w)
}

// With creates a child logger with the field added to its context.
func With() zerolog.Context {
	return logger.With()
}

// Level creates a child logger with the minimum accepted level set to level.
func Level(level zerolog.Level) zerolog.Logger {
	return logger.Level(level)
}

// Sample returns a logger with the s sampler.
func Sample(s zerolog.Sampler) zerolog.Logger {
	return logger.Sample(s)
}

// Hook returns a logger with the h Hook.
func Hook(h zerolog.Hook) zerolog.Logger {
	return logger.Hook(h)
}

// Err starts a new message with error level with err as a field if not nil or
// with info level if err is nil.
//
// You must call Msg on the returned event in order to send the event.
func Err(err error) *zerolog.Event {
	return logger.Err(err)
}

// Trace starts a new message with trace level.
//
// You must call Msg on the returned event in order to send the event.
func Trace() *zerolog.Event {
	return logger.Trace()
}

// Debug starts a new message with debug level.
//
// You must call Msg on the returned event in order to send the event.
func Debug() *zerolog.Event {
	return logger.Debug()
}

// Info starts a new message with info level.
//
// You must call Msg on the returned event in order to send the event.
func Info() *zerolog.Event {
	return logger.Info()
}

// Warn starts a new message with warn level.
//
// You must call Msg on the returned event in order to send the event.
func Warn() *zerolog.Event {
	return logger.Warn()
}

// Error starts a new message with error level.
//
// You must call Msg on the returned event in order to send the event.
func Error() *zerolog.Event {
	return logger.Error()
}

// Fatal starts a new message with fatal level. The os.Exit(1) function
// is called by the Msg method.
//
// You must call Msg on the returned event in order to send the event.
func Fatal() *zerolog.Event {
	return logger.Fatal()
}

// Panic starts a new message with panic level. The message is also sent
// to the panic function.
//
// You must call Msg on the returned event in order to send the event.
func Panic() *zerolog.Event {
	return logger.Panic()
}

// WithLevel starts a new message with level.
//
// You must call Msg on the returned event in order to send the event.
func WithLevel(level zerolog.Level) *zerolog.Event {
	return logger.WithLevel(level)
}

// Log starts a new message with no level. Setting zerolog.GlobalLevel to
// zerolog.Disabled will still disable events produced by this method.
//
// You must call Msg on the returned event in order to send the event.
func Log() *zerolog.Event {
	return logger.Log()
}

// Print sends a log event using debug level and no extra field.
// Arguments are handled in the manner of fmt.Print.
func Print(v ...interface{}) {
	logger.Print(v...)
}

// Printf sends a log event using debug level and no extra field.
// Arguments are handled in the manner of fmt.Printf.
func Printf(format string, v ...interface{}) {
	logger.Printf(format, v...)
}

// Ctx returns the logger associated with the ctx. If no logger
// is associated, a disabled logger is returned.
func Ctx(ctx context.Context) *zerolog.Logger {
	return zerolog.Ctx(ctx)
}

func contextFields(ctx context.Context) (fields map[string]interface{}) {
	fields = make(map[string]interface{})
	if requestID, ok := ctx.Value(constant.ContextKeyRequestID).(uuid.UUID); ok && requestID != uuid.Nil {
		fields["request_id"] = requestID
	}

	if action, ok := ctx.Value(constant.ContextKeyAction).(constant.ActionKey); ok && action != 0 {
		fields["action"] = action.String()
	}
	return fields
}

// With creates a child logger with the field added to its context.
func WithCtx(ctx context.Context) *zerolog.Logger {
	l := With()
	fields := contextFields(ctx)
	l2 := l.Fields(fields).Logger()
	return &l2
}

func defaultLogger() zerolog.Logger {
	fmt.Println("default logger")
	// UNIX Time is faster and smaller than most timestamps
	// If you set zerolog.TimeFieldFormat to an empty string,
	// logs will write with UNIX time
	zerolog.TimeFieldFormat = time.RFC3339Nano
	//  In order to always output a static time to stdout for these
	//  examples to pass, we need to override zerolog.TimestampFunc
	//  and logger.logger globals -- you would not normally need to do this
	// zerolog.TimestampFunc = func() time.Time {
	//	return time.Date(2008, 1, 8, 17, 5, 05, 0, time.UTC)
	// }
	// MessageFieldName is the field name used for the message field.
	zerolog.MessageFieldName = "message"

	// ErrorFieldName is the field name used for error fields.
	zerolog.ErrorFieldName = "error"

	// CallerFieldName is the field name used for caller field.
	zerolog.CallerFieldName = "caller"

	// CallerSkipFrameCount is the number of stack frames to skip to find the caller.
	zerolog.CallerSkipFrameCount = 2

	// CallerMarshalFunc allows customization of global caller marshaling
	zerolog.CallerMarshalFunc = func(file string, line int) string {
		return file + ":" + strconv.Itoa(line)
	}

	var output io.Writer

	// pretty print during development
	out := zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339Nano}
	out.FormatMessage = func(i interface{}) string {
		return fmt.Sprintf("*** %s ***", i)
	}
	out.FormatFieldName = func(i interface{}) string {
		return fmt.Sprintf("%s:", i)
	}
	out.FormatFieldValue = func(i interface{}) string {
		return fmt.Sprintf("%s", i)
	}
	output = out

	l := zerolog.New(output).With().Caller().Timestamp().Logger().Level(zerolog.DebugLevel)

	stdlog.SetFlags(0)
	stdlog.SetOutput(l)
	return l
}
