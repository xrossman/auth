package log

import (
	"fmt"
	"io"
	stdlog "log"
	"os"
	"runtime"
	"strconv"
	"time"

	"github.com/getsentry/raven-go"
	jsoniter "github.com/json-iterator/go"
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
)

type errWithStackTrace struct {
	Err        string            `json:"error"`
	Stacktrace *raven.Stacktrace `json:"stacktrace"`
}

var errSkipEvent = errors.New("skip")

type sentryEvent struct {
	Level     string            `json:"level"`
	Msg       string            `json:"message"`
	Err       errWithStackTrace `json:"error"`
	Time      time.Time         `json:"time"`
	Status    int               `json:"status,omitempty"`
	UserAgent string            `json:"user_agent,omitempty"`
	Method    string            `json:"method,omitempty"`
	URL       string            `json:"url,omitempty"`
	IP        string            `json:"ip,omitempty"`
	RequestID string            `json:"request_id,omitempty"`
	Action    string            `json:"action,omitempty"`
}

// nolint:gocyclo,funlen
// set global Zerolog logger
func NewZerolog(stage string, cfg LogCfg, serviceAlias string, serviceVersion string) (err error) {
	if cfg.Format == "" {
		cfg.Format = "text"
	}
	if cfg.Sentry == nil || cfg.Sentry.DSN == "" || !cfg.Sentry.Enable {
		return newZerolog(cfg, os.Stdout)
	}

	cfg.Format = "json"

	client, err := raven.New(cfg.Sentry.DSN)
	if err != nil {
		return err
	}

	pr, pw := io.Pipe()

	go func() {
		defer client.Close()

		var json = jsoniter.ConfigCompatibleWithStandardLibrary
		dec := json.NewDecoder(pr)

		for {
			var e sentryEvent

			err := dec.Decode(&e)

			switch err {
			case nil:
				break
			case io.EOF:
				return
			case errSkipEvent:
				continue
			default:
				_, _ = fmt.Fprintf(os.Stderr, "unmarshaling logger failed with error %v\n", err)
				continue
			}

			var level raven.Severity

			switch e.Level {
			case "debug":
				level = raven.DEBUG
			case "info":
				level = raven.INFO
			case "warn":
				level = raven.WARNING
			case "error":
				level = raven.ERROR
			case "fatal", "panic":
				level = raven.FATAL
			default:
				continue
			}

			packet := raven.Packet{
				Message:     e.Msg,
				Timestamp:   raven.Timestamp(e.Time),
				Level:       level,
				Platform:    "go",
				Project:     serviceAlias,
				Logger:      cfg.Type,
				Release:     serviceVersion,
				Culprit:     e.Err.Err,
				Environment: stage,
			}

			if e.Err.Stacktrace != nil {
				packet.Interfaces = append(packet.Interfaces, e.Err.Stacktrace)
			}

			if e.IP != "" {
				packet.Interfaces = append(packet.Interfaces, &raven.User{IP: e.IP})
			}

			if e.URL != "" {
				h := raven.Http{
					URL:     e.URL,
					Method:  e.Method,
					Headers: make(map[string]string),
				}
				if e.UserAgent != "" {
					h.Headers["User-Agent"] = e.UserAgent
				}

				packet.Interfaces = append(packet.Interfaces, &h)
			}

			_, _ = client.Capture(&packet, nil)
		}
	}()

	return newZerolog(cfg, io.MultiWriter(os.Stdout, pw))
}

type stackTracer interface {
	StackTrace() errors.StackTrace
}

func stackTraceToSentry(st errors.StackTrace) *raven.Stacktrace {
	var frames []*raven.StacktraceFrame

	for _, f := range st {
		pc := uintptr(f) - 1 // nolint:gomnd
		fn := runtime.FuncForPC(pc)

		var (
			funcName, file string
			line           int
		)

		if fn != nil {
			file, line = fn.FileLine(pc)
			funcName = fn.Name()
		} else {
			file = "unknown"
			funcName = "unknown"
		}

		frame := raven.NewStacktraceFrame(pc, funcName, file, line, 3, nil)
		if frame != nil {
			frames = append([]*raven.StacktraceFrame{frame}, frames...)
		}
	}

	return &raven.Stacktrace{Frames: frames}
}

// nolint:funlen
func newZerolog(cfg LogCfg, w io.Writer) (err error) {
	// setup a global function that transforms any error passed to
	// zerolog to an error with stack strace.
	zerolog.ErrorMarshalFunc = func(err error) interface{} {
		if cfg.Sentry == nil {
			return err
		}
		if !cfg.Sentry.StackTrace.Enable {
			return err
		}

		es := errWithStackTrace{
			Err: err.Error(),
		}

		if _, ok := err.(stackTracer); !ok {
			err = errors.WithStack(err)
		}

		es.Stacktrace = stackTraceToSentry(err.(stackTracer).StackTrace())

		return &es
	}

	// UNIX Time is faster and smaller than most timestamps
	// If you set zerolog.TimeFieldFormat to an empty string,
	// logs will write with UNIX time
	zerolog.TimeFieldFormat = time.RFC3339Nano
	//  In order to always output a static time to stdout for these
	//  examples to pass, we need to override zerolog.TimestampFunc
	//  and logger.logger globals -- you would not normally need to do this
	// zerolog.TimestampFunc = func() time.Time {
	//	return time.Date(2008, 1, 8, 17, 5, 05, 0, time.UTC)
	// }
	// MessageFieldName is the field name used for the message field.
	zerolog.MessageFieldName = "message"

	// ErrorFieldName is the field name used for error fields.
	zerolog.ErrorFieldName = "error"

	// CallerFieldName is the field name used for caller field.
	zerolog.CallerFieldName = "caller"

	// CallerSkipFrameCount is the number of stack frames to skip to find the caller.
	zerolog.CallerSkipFrameCount = 2

	// CallerMarshalFunc allows customization of global caller marshaling
	zerolog.CallerMarshalFunc = func(file string, line int) string {
		return file + ":" + strconv.Itoa(line)
	}

	level, err := zerolog.ParseLevel(cfg.Level)
	if err != nil {
		return err
	}

	var output io.Writer

	switch cfg.Format {
	case "text":
		// pretty print during development
		out := zerolog.ConsoleWriter{Out: w, TimeFormat: time.RFC3339Nano}
		out.FormatMessage = func(i interface{}) string {
			return fmt.Sprintf("*** %s ***", i)
		}
		out.FormatFieldName = func(i interface{}) string {
			return fmt.Sprintf("%s:", i)
		}
		out.FormatFieldValue = func(i interface{}) string {
			return fmt.Sprintf("%s", i)
		}
		output = out
	default:
		output = w
	}

	logger = zerolog.New(output).With().Caller().Timestamp().Logger().Level(level)

	stdlog.SetFlags(0)
	stdlog.SetOutput(logger)

	return nil
}
