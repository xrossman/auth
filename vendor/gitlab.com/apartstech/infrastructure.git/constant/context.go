package constant

import "time"

// noinspection GoUnusedConst
const (
	// Common
	GeoWGS84 = 4326 // WGS84 spheroid

	// Services
	TimeoutShutdown = 100 * time.Millisecond

	// Stages
	StageDefaults    = "defaults"
	StageDevelopment = "development"
	StageLocal       = "local"
	StageProduction  = "production"
	StageTesting     = "testing"
)

// noinspection GoUnusedConst
const (
	// API Actions
	ActionUnknown ActionKey = iota
	ActionError
	ActionHealthCheck
	ActionSignIn
)

type ActionKey int64

func (ak ActionKey) String() string {
	return [...]string{
		"Unknown",
		"Error",
		"Health Check",
		"SignIn",
	}[ak]
}

type ContextKey int64

const (
	// Context
	ContextKeyRequestID ContextKey = iota + 1
	ContextKeyAction
	ContextKeyHTTPStatus
	ContextKeyUserAgent
	ContextKeyMethod
	ContextKeyURL
	ContextKeyIP
)

func (ck ContextKey) String() string {
	return [...]string{
		"Request ID",
		"Action",
		"HTTP Status",
		"HTTP UserAgent",
		"HTTP Method",
		"URL",
		"IP",
	}[ck-1]
}
