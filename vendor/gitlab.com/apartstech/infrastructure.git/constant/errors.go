package constant

import "errors"

var (
	ErrorConflict = errors.New("conflict ")
)
