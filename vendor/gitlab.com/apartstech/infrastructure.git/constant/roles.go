package constant

const (
	CasbinDomainMain = "entrypoint"

	CasbinRoleMain     = "user"
	CasbinRoleRegister = "user_waiting"
	CasbinRoleRealtor  = "realtor"
	CasbinRoleAdmin    = "admin"
)
