module github.com/spacetab-io/configuration-go

go 1.14

require (
	github.com/imdario/mergo v0.3.10-0.20200517151347-9080aaff8536
	github.com/stretchr/testify v1.5.1
	gopkg.in/yaml.v2 v2.3.0
)
