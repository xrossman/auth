package router

import (
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/logger"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"gitlab.com/apartstech/infrastructure.git/log"

	"gitlab.com/apartstech/layer2/auth.git/v1/app/handler"
	"gitlab.com/apartstech/layer2/auth.git/v1/app/middleware"
	"gitlab.com/apartstech/layer2/auth.git/v1/cfg"
)

func InitRouter() *gin.Engine {
	r := gin.Default()

	r.Use(cors.New(cors.Config{
		AllowAllOrigins: true,
		AllowMethods:    []string{"GET", "PUT", "POST", "DELETE", "OPTIONS"},
		AllowHeaders:    []string{"Origin", "Content-Type", "Authorization", "Access-Control-Request-Method", "Access-Control-Allow-Origin"},
	}))

	if gin.IsDebugging() {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	sublog := log.Logger().With().CallerWithSkipFrameCount(11).Logger()

	r.Use(logger.SetLogger(logger.Config{
		Logger: &sublog,
		UTC:    true,
	}))

	r.GET("/", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, cfg.Store.ServiceDescription)
	})

	api := r.Group("/api/v1")
	{
		authHandler := handler.AuthHandlers{}
		oauthHandler := handler.NewOAuthController() // Merge with AuthHandler?
		userHandler := handler.User{}
		finHandler := handler.FinancialHandler{}

		api.POST("/subscribe", authHandler.SubscribeHandler)

		auth := api.Group("/auth")
		{
			auth.POST("/signup/stepone", authHandler.SignUpStepOne)
			auth.GET("/signup/steptwo", authHandler.SigUpStepTwo)
			auth.POST("/renew", authHandler.RenewToken)
			auth.POST("/signin", authHandler.SignIn)
			oauth := auth.Group("/o")
			{
				oauth.GET("", oauthHandler.Root)
				oauth.GET("/:provider/login", oauthHandler.Login)
				oauth.GET("/:provider/logout", oauthHandler.Logout)

				oauth.GET("/:provider/callback", oauthHandler.Callback)
			}
		}

		// realtor
		realtor := api.Group("/r")
		{
			realtor.GET("/:display_name", userHandler.ReadRealtor)
		}

		// financial
		fin := api.Group("/financial")
		{
			fin.POST("/convert", finHandler.ConvertCurrencyHandler)
		}

		// Profile
		profile := api.Group("/profile").Use(middleware.Authorizer)
		{
			profile.GET("", userHandler.Read) // get logged user detail
			profile.PUT("", userHandler.Update)
			profile.DELETE("", userHandler.Delete)
			profile.PUT("/password", userHandler.PasswordUpdate)
			profile.POST("/avatar", userHandler.SetAvatar)
		}

		// Search Users
		search := api.Group("/search")
		{
			search.POST("", userHandler.Search) // get user detail by filter
		}

		apartHandler := handler.NewApartmentsHandler()

		api.GET("/show/apartments/:apartment_id", apartHandler.ReadApartment)
		api.GET("/list/apartments", apartHandler.ListApartments)
		api.GET("/list/apartments/meta", apartHandler.SearchMetaData)
		api.GET("/offer_by_hash/:offer_hash", apartHandler.ReadOfferByHashHandler)
		api.GET("/reference_books/amenities", apartHandler.ListAmenities)
		api.GET("/reference_books/ranks_n_types", apartHandler.PropertyRanksAndTypes)

		api.Use(middleware.Authorizer)
		{
			api.POST("/apartments", apartHandler.CreateApartmentTemplate)
			api.POST("/apartments/:apartment_id/image", apartHandler.AddImage)
			api.DELETE("/apartments/:apartment_id/:image_name/image", apartHandler.DeleteImage)
			api.GET("/apartments/:apartment_id", apartHandler.ReadApartment)
			api.GET("/apartments", apartHandler.ListMyApartments)
			api.PUT("/apartments/:apartment_id", apartHandler.UpdateApartment)
			api.PUT("/apartments/:apartment_id/publish", apartHandler.Publish)
			api.DELETE("/apartments/:apartment_id", apartHandler.ArchiveApartment)

			api.POST("/room/:apartment_id", apartHandler.CreateRoom)
			api.GET("/room/:apartment_id/:room_id", apartHandler.ReadRoom)
			api.GET("/rooms/:apartment_id", apartHandler.ReadApartmentsRooms)
			api.PUT("/room/:apartment_id/:room_id", apartHandler.UpdateRoom)
			api.DELETE("/room/:apartment_id/:room_id", apartHandler.DeleteRoom)

			api.POST("/offers", apartHandler.CreateOfferHandler)
			api.GET("/offers", apartHandler.ListOffersHandler)
			api.GET("/offers/:offer_id", apartHandler.ReadOfferByIDHandler)
			api.PUT("/offers/:offer_id", apartHandler.UpdateOfferHandler)
			api.DELETE("/offers/:offer_id", apartHandler.DeleteOfferHandler)

			placesHandler := handler.PlacesHandler{}
			api.GET("/maps/places/:query", placesHandler.HandleSearchPlaces)
			api.GET("/maps/geocoding/address/:query", placesHandler.HandleAddressGeocoding)
			api.GET("/maps/geocoding/reverse", placesHandler.HandleLatLngGeocoding)
			api.GET("/maps/autocomplete/:query", placesHandler.HandleAutocomplete)
			api.GET("/maps/details/:place_id", placesHandler.HandlePlaceDetails)

			translateHandler := &handler.TranslationsHandler{}
			api.POST("/translate", translateHandler.TranslateText)
		}

		admin := api.Group("/admin")
		admin.Use(middleware.Authorizer)
		{
			admin.POST("/search", userHandler.AdminSearch)

			user := admin.Group("/user")
			{
				user.GET("", userHandler.List)
				user.POST("", userHandler.Create)
				user.DELETE("", userHandler.Delete)
				user.GET("/:userID", userHandler.Read)
				user.PUT("/:userID", userHandler.Update)
				user.POST("/:userID/password", userHandler.AdminResetUserPassword)
				user.POST("/:userID/role/:role", userHandler.AdminSetUserRole)
				user.DELETE("/:userID/role/:role", userHandler.AdminDeleteUserRole)
			}

			roles := admin.Group("/roles")
			{
				roles.GET("", authHandler.RoleList)
				roles.POST("", authHandler.RoleCreate)       // get user detail by filter
				roles.DELETE("", authHandler.RoleMarkDelete) // MarkDelete
			}

			property := admin.Group("/property")
			{
				property.GET("", apartHandler.AdminSearchForProperties)
				property.PUT("/:propID/status/:status", apartHandler.SetPropertyStatus)
				property.GET("/:realtorID", apartHandler.AdminReadProperties)
			}
		}
	}

	//tools := r.Group("/tools")
	//{
	//	tools.Any("/metrics/*path", middleware.BasicAuth(), ginproxy.ReverseProxy(cfg.Store.ProxyTargets.Grafana))
	//	tools.Any("/tracing/*path", middleware.BasicAuth(), ginproxy.ReverseProxy(cfg.Store.ProxyTargets.Jaeger))
	//}

	return r
}
