package middleware

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

var xForwardedFor = http.CanonicalHeaderKey("X-Forwarded-For")
var xRealIP = http.CanonicalHeaderKey("X-Real-IP")

func RealIP(ctx *gin.Context) (ip string) {
	if xrip := ctx.Request.Header.Get(xRealIP); xrip != "" {
		ip = xrip
	} else if xff := ctx.Request.Header.Get(xForwardedFor); xff != "" {
		i := strings.Index(xff, ", ")
		if i == -1 {
			i = len(xff)
		}
		ip = xff[:i]
	}

	return ip
}
