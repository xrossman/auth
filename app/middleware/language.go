package middleware

import (
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/go-http-utils/headers"
)

const (
	defaultLanguage = "en"
)

var availableLanguages = map[string]string{
	"ru-RU": "ru",
	"en-US": "en",
	"es-ES": "es",
}

func DefineLanguage(c *gin.Context) {
	var (
		lang string = defaultLanguage
		ok   bool
	)

	contentLanguage := c.GetHeader(headers.ContentLanguage)
	acceptLanguage := c.GetHeader(headers.AcceptLanguage)
	xLanguage := c.GetHeader("X-Language")

	var langs []string

	// Content Language - язык в настройках приложения
	if contentLanguage != "" {
		if strings.Contains(contentLanguage, ",") {
			langs = strings.Split(contentLanguage, ",")
		} else {
			langs = append(langs, contentLanguage)
		}
	}

	// Accept Language - язык в локали
	if acceptLanguage != "" {
		if strings.Contains(acceptLanguage, ",") {
			l := strings.Split(acceptLanguage, ",")
			langs = append(langs, l...)
		} else {
			langs = append(langs, acceptLanguage)
		}
	}

	// X-Language - наш хидер
	if xLanguage != "" {
		if strings.Contains(xLanguage, ",") {
			l := strings.Split(xLanguage, ",")
			langs = append(langs, l...)
		} else {
			langs = append(langs, xLanguage)
		}
	}

	for _, l := range langs {
		if lang, ok = availableLanguages[l]; ok {
			break
		}
	}

	c.Set("lang", lang)
	c.Next()
}
