package middleware

import (
	"context"
	"encoding/base64"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/apartstech/infrastructure.git/log"
	"gitlab.com/apartstech/proto.git/custom"
	proto "gitlab.com/apartstech/proto.git/pkg/models/aclmodels"
	"google.golang.org/grpc/status"

	"gitlab.com/apartstech/layer2/auth.git/v1/cfg"
	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/grpc"
)

const (
	LoggedUserContextValue = "logged_user"
	RequesterContextValue  = "requester"
)

func Authorizer(c *gin.Context) {
	r, err := protoCheck(c, cfg.Store.ServiceDescription.ServiceAlias, c.Request.URL.Path, c.Request.Method)
	if err != nil || !r.GetAllowed() {
		if err != nil {
			st := status.Convert(err)
			if st.Code() != 0 {
				log.Error().Err(err).Msgf("grpc error")
				c.AbortWithStatusJSON(http.StatusInternalServerError, map[string]string{"error": "access server is down"})
				return
			}
			log.Warn().Msgf("Check Allowed error: %#v", err)
			c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{"error": err.Error()})
		} else {
			c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{"error": "not allowed"})
		}
		return
	}

	c.Set(LoggedUserContextValue, r.GetUser())
	c.Set(RequesterContextValue, r.GetUser().UUID)
	c.Next()
}

func BasicAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
		auth := strings.SplitN(c.Request.Header.Get("Authorization"), " ", 2)

		if len(auth) != 2 || auth[0] != "Basic" {
			c.Writer.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		payload, _ := base64.StdEncoding.DecodeString(auth[1])
		pair := strings.SplitN(string(payload), ":", 2)

		r, err := grpc.GetACLClient().SignIn(c, &proto.LoginRequest{
			Login:    pair[0],
			Password: pair[1],
			Ip:       RealIP(c),
		})
		if err != nil {
			c.JSON(http.StatusUnauthorized, err)
			c.Abort()
			return
		}

		c.Set("logged_user", r.GetUser())
		c.Set("requester", r.GetUser().UUID)
		fmt.Println("NEXT")
		c.Next()
	}
}

func protoCheck(ctx *gin.Context, domain, path, method string) (*proto.UserAllowedResponse, error) {
	authHeader := ctx.GetHeader("Authorization")
	if authHeader == "" {
		return nil, fmt.Errorf("empty token provided")
	}

	authValues := strings.Split(authHeader, " ")
	if len(authValues) < 2 {
		return nil, fmt.Errorf("empty token provided")
	}

	ctxC, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	c := grpc.GetACLClient()
	r, err := c.UserAllowed(ctxC, &proto.UserAllowedRequest{
		Token:  authValues[1],
		Ip:     RealIP(ctx),
		Domain: domain,
		Path:   path,
		Method: method,
	})

	if err != nil {
		log.Error().Err(err).Msgf("Allowed: %v", r.Allowed)
	}

	return r, err
}

func GetRequesterIDFromContext(c *gin.Context) (*custom.UUID, error) {
	value := c.GetString(RequesterContextValue)
	if value == "" {
		return nil, fmt.Errorf("requester context value isn't set")
	}

	return custom.ParseCustomUUID(value)
}

func GetLoggedUserFromContext(c *gin.Context) (*proto.User, error) {
	value, exists := c.Get(LoggedUserContextValue)
	if !exists {
		return nil, fmt.Errorf("logged user context value isn't set")
	}

	if user, ok := value.(*proto.User); ok {
		return user, nil
	}

	return nil, fmt.Errorf("logged user value has unsuported type %T", value)
}
