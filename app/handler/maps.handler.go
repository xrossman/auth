package handler

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	ginerrors "github.com/microparts/errors-go-gin"
	"gitlab.com/apartstech/infrastructure.git/log"
	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/grpc"
	"gitlab.com/apartstech/proto.git/pkg/models/mapsmodels"
)

type PlacesHandler struct{}

func (ph PlacesHandler) HandleSearchPlaces(c *gin.Context) {
	q := c.Param("query")

	if q == "" {
		c.Next()
		return
	}

	message := &mapsmodels.PlaceSearchQueryMessage{
		Query: q,
	}

	data, err := grpc.GetMapsClient().Search(c, message)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	result, err := mapsmodels.FromPBToHumanPlaceSearchResponse(data)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}

func (ph PlacesHandler) HandleAddressGeocoding(c *gin.Context) {
	q := c.Param("query")

	if q == "" {
		c.Next()
		return
	}

	message := &mapsmodels.GeocodingAddressRequestMessage{
		Query:    q,
		Language: c.GetString("lang"),
	}

	data, err := grpc.GetMapsClient().AddressGeocoding(c, message)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	result, err := mapsmodels.FromPBToHumanGeocodingResult(data)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}

func (ph PlacesHandler) HandleLatLngGeocoding(c *gin.Context) {
	latValue := c.Query("lat")
	lat, err := strconv.ParseFloat(latValue, 64)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	lngValue := c.Query("lng")
	lng, err := strconv.ParseFloat(lngValue, 64)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	message := &mapsmodels.GeocodingLatLngRequestMessage{
		Lat:      lat,
		Lng:      lng,
		Language: c.GetString("lang"),
	}

	data, err := grpc.GetMapsClient().LatLngGeocoding(c, message)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	result, err := mapsmodels.FromPBToHumanGeocodingResult(data)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}

func (ph PlacesHandler) HandleAutocomplete(c *gin.Context) {
	var (
		radius   uint64
		lat, lng float64
		err      error
	)
	q := c.Param("query")

	if q == "" {
		c.Next()
		return
	}

	sessionToken := c.Query("session_token")
	placeType := c.Query("place_type")

	latValue := c.Query("lat")
	if latValue != "" {
		lat, err = strconv.ParseFloat(latValue, 64)
		if err != nil {
			ginerrors.Response(c, err)
			return
		}
	}

	radiusValue := c.Query("radius")
	if radiusValue != "" {
		radius, err = strconv.ParseUint(radiusValue, 0, 64)
		if err != nil {
			ginerrors.Response(c, err)
			return
		}
	}

	lngValue := c.Query("lng")
	if lngValue != "" {
		lng, err = strconv.ParseFloat(lngValue, 64)
		if err != nil {
			ginerrors.Response(c, err)
			return
		}
	}

	message := &mapsmodels.PlacesAutocompleteRequestMessage{
		Query:        q,
		SessionToken: sessionToken,
		PlaceType:    placeType,
		Lat:          lat,
		Lng:          lng,
		Radius:       radius,
		Language:     c.GetString("lang"),
	}

	data, err := grpc.GetMapsClient().Autocomplete(c, message)
	if err != nil {
		log.Err(err).Send()
		ginerrors.Response(c, err)
		return
	}

	result, err := mapsmodels.FromPBToHumanPlacesAutoCompleteResponse(data)
	if err != nil {
		log.Err(err).Send()
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}

func (ph PlacesHandler) HandlePlaceDetails(c *gin.Context) {
	id := c.Param("place_id")

	if id == "" {
		c.Next()
		return
	}

	sessionToken := c.Query("session_token")
	message := &mapsmodels.PlaceDetailsRequestMessage{
		PlaceID:      id,
		SessionToken: sessionToken,
		Language:     c.GetString("lang"),
	}

	data, err := grpc.GetMapsClient().Details(c, message)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	result, err := mapsmodels.FromPBToHumanPlaceDetailsResult(data)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}
