package handler

import (
	"fmt"
	"net/http"
	"strconv"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	ginerrors "github.com/microparts/errors-go-gin"
	"gitlab.com/apartstech/infrastructure.git/log"
	"gitlab.com/apartstech/layer2/auth.git/v1/app/middleware"
	"gitlab.com/apartstech/layer2/auth.git/v1/domain/commands"
	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/grpc"
	"gitlab.com/apartstech/proto.git/pkg/models"
	"gitlab.com/apartstech/proto.git/pkg/models/apartmentsmodels"
	"gitlab.com/apartstech/proto.git/pkg/models/filesmodels"
)

type ApartmentsHandler struct {
	typesAndRanks map[string][]string
}

func (h ApartmentsHandler) CreateApartment(c *gin.Context) {
	var req apartmentsmodels.CreateApartmentRequest

	if err := c.ShouldBindJSON(&req); err != nil {
		log.Error().Err(err).Msgf("[CREATE APARTMENT] can't bind request: %v", err)
		ginerrors.Response(c, err)
		return
	}

	req.Apartment.RealtorID = c.GetString("requester")
	result, err := grpc.GetApartmentsClient().CreateApartment(c, &req)
	if err != nil {
		log.Error().Err(err).Msgf("[CREATE APARTMENT] can't create property: %v", err)
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}

func (h ApartmentsHandler) CreateApartmentTemplate(c *gin.Context) {
	realtorID := c.GetString(middleware.RequesterContextValue)

	user, err := middleware.GetLoggedUserFromContext(c)
	if err != nil {
		log.Error().Err(err).Msgf("[CREATE APARTMENT TEMPLATE] can't get user from context: %v", err)
		ginerrors.Response(c, err)
		return
	}

	result, err := grpc.GetApartmentsClient().CreateApartmentTemplate(c, &apartmentsmodels.CreateApartmentTemplateRequest{
		RealtorID: realtorID,
	})
	if err != nil {
		log.Error().Err(err).Msgf("[CREATE APARTMENT TEMPLATE] can't create template: %v", err)
		ginerrors.Response(c, err)
		return
	}

	result.Apartment.Realtor = user

	c.JSON(http.StatusOK, result)
}

func (h ApartmentsHandler) ReadApartment(c *gin.Context) {
	apartmentIDValue := c.Param("apartment_id")

	var (
		apartID int
		err     error
	)

	if apartID, err = strconv.Atoi(apartmentIDValue); err != nil {
		log.Error().Err(err).Msgf("[READ APARTMENT] can't parse property ID value: %v", err)
		ginerrors.Response(c, err)
		return
	}
	result, err := commands.ReadApartmentByIDCommand(uint64(apartID))
	if err != nil {
		log.Error().Err(err).Msgf("[READ APARTMENT] can't read property: %v", err)
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}

func (h ApartmentsHandler) Publish(c *gin.Context) {
	var req apartmentsmodels.UpdateApartmentRequest

	apartmentIDValue := c.Param("apartment_id")

	var (
		apartID int
		err     error
	)

	if apartID, err = strconv.Atoi(apartmentIDValue); err != nil {
		log.Error().Err(err).Msgf("[PUBLISH] can't parse property ID value: %v", err)
		ginerrors.Response(c, err)
		return
	}

	if err := c.ShouldBindJSON(&req); err != nil {
		log.Error().Err(err).Msgf("[PUBLISH] can't bind request: %v", err)
		ginerrors.Response(c, err)
		return
	}

	req.ApartmentID = uint64(apartID)

	req.Apartment.RealtorID = c.GetString("requester")
	req.ForPublish = true
	status := apartmentsmodels.ApartmentStatus_published
	acceptedAt := time.Now()
	req.Apartment.Status = apartmentsmodels.ApartmentStatusObject{
		Status:     &status,
		AcceptedAt: &acceptedAt,
	}
	result, err := commands.UpdateApartmentCommand(&req)
	if err != nil {
		log.Error().Err(err).Msgf("[PUBLISH] can't publish property: %v", err)
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}

func (h ApartmentsHandler) UpdateApartment(c *gin.Context) {
	var req apartmentsmodels.UpdateApartmentRequest

	apartmentIDValue := c.Param("apartment_id")

	var (
		apartID int
		err     error
	)

	if apartID, err = strconv.Atoi(apartmentIDValue); err != nil {
		log.Error().Err(err).Msgf("[UPDATE APARTMENT] can't parse property ID value: %v", err)
		ginerrors.Response(c, err)
		return
	}

	if err := c.ShouldBindJSON(&req); err != nil {
		log.Error().Err(err).Msgf("[UPDATE APARTMENT] can't bind request: %v", err)
		ginerrors.Response(c, err)
		return
	}

	req.ApartmentID = uint64(apartID)

	req.Apartment.RealtorID = c.GetString("requester")

	_, err = commands.UpdateApartmentCommand(&req)
	if err != nil {
		log.Error().Err(err).Msgf("[UPDATE APARTMENT] can't update property: %v", err)
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, nil)
}

func (h ApartmentsHandler) ArchiveApartment(c *gin.Context) {
	apartmentIDValue := c.Param("apartment_id")

	var (
		apartID int
		err     error
	)

	if apartID, err = strconv.Atoi(apartmentIDValue); err != nil {
		log.Error().Err(err).Msgf("[ARCHIVE APARTMENT] can't parse property ID value: %v", err)
		ginerrors.Response(c, err)
		return
	}

	realtorID := c.GetString("requester")

	req := apartmentsmodels.DeleteApartmentRequest{
		RealtorID:   realtorID,
		ApartmentID: uint64(apartID),
	}

	result, err := grpc.GetApartmentsClient().ArchiveApartment(c, &req)
	if err != nil {
		log.Error().Err(err).Msgf("[ARCHIVE APARTMENT] can't archive property: %v", err)
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}

func (h ApartmentsHandler) ListApartments(c *gin.Context) {
	filter, err := apartmentsmodels.NewApartmentsFilterFromContext(c)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	filter.Statuses = nil
	isTemplate := apartmentsmodels.ApartmentStatus_template
	isArchived := apartmentsmodels.ApartmentStatus_archived
	isBlocked := apartmentsmodels.ApartmentStatus_blocked
	filter.ExcludeStatuses = append(filter.ExcludeStatuses, isTemplate.String(), isArchived.String(), isBlocked.String())

	p, err := models.NewOffsetLimitPaginationFromGinContext(c)
	if err != nil {
		log.Error().Err(err).Msgf("[LIST APARTMENTS] can't bind pagination: %v", err)
		ginerrors.Response(c, err)
		return
	}

	result, err := commands.ListApartmentsCommand(filter, p)
	if err != nil {
		log.Error().Err(err).Msgf("[LIST APARTMENTS] received error status: %v", err)
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}

func (h ApartmentsHandler) ListMyApartments(c *gin.Context) {
	filter, err := apartmentsmodels.NewApartmentsFilterFromContext(c)
	if err != nil {
		log.Error().Err(err).Msgf("[LIST MY APARTMENTS] can't bind filter: %v", err)
		ginerrors.Response(c, err)
		return
	}

	requester, err := middleware.GetRequesterIDFromContext(c)
	if err != nil {
		log.Error().Err(err).Msgf("[LIST MY APARTMENTS] can't bind pagination: %v", err)
		ginerrors.Response(c, err)
		return
	}

	filter.RealtorsIDS = []string{requester.String()}

	p, err := models.NewOffsetLimitPaginationFromGinContext(c)
	if err != nil {
		log.Error().Err(err).Msgf("[LIST MY APARTMENTS] can't bind pagination: %v", err)
		ginerrors.Response(c, err)
		return
	}

	result, err := commands.ListApartmentsCommand(filter, p)
	if err != nil {
		log.Error().Err(err).Msgf("[LIST MY APARTMENTS] can't list apartments: %v", err)
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}

func (h ApartmentsHandler) AddImage(c *gin.Context) {
	apartmentIDValue := c.Param("apartment_id")
	apartmentID, err := strconv.Atoi(apartmentIDValue)
	if err != nil {
		log.Error().Err(err).Msgf("[ADD IMAGE] can't parse property ID value: %v", err)
		ginerrors.Response(c, err)
		return
	}

	data, err := c.GetRawData()
	if err != nil {
		log.Error().Err(err).Msgf("[ADD IMAGE] can't get image data: %v", err)
		ginerrors.Response(c, err)
		return
	}

	name, err := commands.AddImageToApartment(data, uint64(apartmentID))
	if err != nil {
		log.Error().Err(err).Msgf("[ADD IMAGE] can't add image: %v", err)
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{"name": name})
}

func (h ApartmentsHandler) DeleteImage(c *gin.Context) {
	apartmentIDValue := c.Param("apartment_id")
	apartmentID, err := strconv.Atoi(apartmentIDValue)

	if err != nil {
		log.Error().Err(err).Msgf("[DELETE IMAGE] can't parse property ID value: %v", err)
		ginerrors.Response(c, err)
		return
	}

	name := c.Param("image_name")
	err = commands.DeleteImage(filesmodels.FileCategory_apartment.String(), name)
	if err != nil {
		log.Error().Err(err).Msgf("[DELETE IMAGE] can't delete image from bucket: %v", err)
		ginerrors.Response(c, err)
		return
	}

	err = commands.DeleteImageFromApartment(name, uint64(apartmentID))
	if err != nil {
		log.Error().Err(err).Msgf("[DELETE IMAGE] can't delete image from property: %v", err)
		ginerrors.Response(c, err)
		return
	}
	c.Status(http.StatusOK)
}

func (h ApartmentsHandler) SearchMetaData(c *gin.Context) {
	filter, err := apartmentsmodels.NewApartmentsFilterFromContext(c)
	if err != nil {
		log.Error().Err(err).Msgf("[SEARCH META DATA] can't bind filter: %v", err)
		ginerrors.Response(c, err)
		return
	}

	result, err := commands.SearchMetaDataCommand(filter)
	if err != nil {
		log.Error().Err(err).Msgf("[SEARCH META DATA] can't read meta data: %v", err)
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}

func (h ApartmentsHandler) SetPropertyStatus(c *gin.Context) {
	idValue := c.Param("propID")
	id, err := strconv.ParseUint(idValue, 0, 64)
	if err != nil {
		log.Error().Err(err).Msgf("[SET PROPERTY STATUS] can't parse property ID value: %v", err)
		ginerrors.Response(c, err)
		return
	}

	statusValue := c.Param("status")

	result, err := commands.SetPropertyStatus(id, statusValue)
	if err != nil {
		log.Error().Err(err).Msgf("[SET PROPERTY STATUS] can't set property status: %v", err)
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}

func (h ApartmentsHandler) AdminReadProperties(c *gin.Context) {
	realtorID := c.Param("realtorID")
	if realtorID == "" {
		log.Error().Msg("[ADMIN READ PROPERTIES] realtor ID cannot be empty")
		ginerrors.Response(c, fmt.Errorf("realtor ID can't be empty"))
		return
	}

	filter, err := apartmentsmodels.NewApartmentsFilterFromContext(c)
	if err != nil {
		log.Error().Msgf("[ADMIN READ PROPERTIES] can't bind filter: %v", err)
		ginerrors.Response(c, err)
		return
	}

	filter.RealtorsIDS = []string{realtorID}

	result, err := commands.ListApartmentsCommand(filter, nil)
	if err != nil {
		log.Error().Msgf("[ADMIN READ PROPERTIES] can't list properties: %v", err)
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}

func (h ApartmentsHandler) AdminSearchForProperties(c *gin.Context) {
	filter, err := apartmentsmodels.NewApartmentsFilterFromContext(c)
	if err != nil {
		log.Error().Msgf("[ADMIN SEARCH FOR PROPERTIES] can't bind filter: %v", err)
		ginerrors.Response(c, err)
		return
	}

	p, err := models.NewOffsetLimitPaginationFromGinContext(c)
	if err != nil {
		log.Error().Msgf("[ADMIN SEARCH FOR PROPERTIES] can't bind pagination: %v", err)
		ginerrors.Response(c, err)
		return
	}

	result, err := commands.ListApartmentsCommand(filter, p)
	if err != nil {
		log.Error().Msgf("[ADMIN SEARCH FOR PROPERTIES] can't list properties: %v", err)
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}

func NewApartmentsHandler() *ApartmentsHandler {
	a := &ApartmentsHandler{}
	mu := sync.Mutex{}
	a.typesAndRanks = make(map[string][]string)

	wg := &sync.WaitGroup{}
	wg.Add(2)
	go func(wg *sync.WaitGroup) {
		result := make([]string, 0)
		for _, v := range apartmentsmodels.ApartmentRank_name {
			result = append(result, v)
		}

		mu.Lock()
		a.typesAndRanks["ranks"] = result
		mu.Unlock()

		wg.Done()
	}(wg)

	go func(wg *sync.WaitGroup) {
		result := make([]string, 0)
		for _, v := range apartmentsmodels.ApartmentType_name {
			result = append(result, v)
		}

		mu.Lock()
		a.typesAndRanks["types"] = result
		mu.Unlock()

		wg.Done()
	}(wg)

	wg.Wait()

	return a
}
