package handler

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	ginerrors "github.com/microparts/errors-go-gin"
	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/grpc"
	"gitlab.com/apartstech/proto.git/pkg/models"
	"gitlab.com/apartstech/proto.git/pkg/models/apartmentsmodels"
)

func (h ApartmentsHandler) CreateOfferHandler(c *gin.Context) {
	var req apartmentsmodels.CreateOfferRequest

	if err := c.ShouldBindJSON(&req); err != nil {
		ginerrors.Response(c, err)
		return
	}

	req.RealtorID = c.GetString("requester")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)

	defer cancel()

	result, err := grpc.GetApartmentsClient().CreateOffer(ctx, &req)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}

func (h ApartmentsHandler) ReadOfferByIDHandler(c *gin.Context) {
	offerIDValue := c.Param("offer_id")

	var (
		offerID int
		err     error
	)

	if offerID, err = strconv.Atoi(offerIDValue); err != nil {
		ginerrors.Response(c, err)
		return
	}

	req := &models.UintIDRequest{
		ID: uint64(offerID),
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)

	defer cancel()

	result, err := grpc.GetApartmentsClient().ReadOfferByID(ctx, req)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}
func (h ApartmentsHandler) ReadOfferByHashHandler(c *gin.Context) {
	offerHash := c.Param("offer_hash")

	req := &apartmentsmodels.ReadOfferByHashRequest{
		OfferHash: offerHash,
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)

	defer cancel()

	result, err := grpc.GetApartmentsClient().ReadOfferByHash(ctx, req)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}

func (h ApartmentsHandler) ListOffersHandler(c *gin.Context) {
	p, err := models.NewPaginationFromGinContext(c)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	req := &apartmentsmodels.ListOffersRequest{
		OwnerID:    c.GetString("requester"),
		Pagination: p,
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)

	defer cancel()

	result, err := grpc.GetApartmentsClient().ListOffers(ctx, req)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}
func (h ApartmentsHandler) UpdateOfferHandler(c *gin.Context) {
	offerIDValue := c.Param("offer_id")

	var (
		offerID int
		err     error
	)

	if offerID, err = strconv.Atoi(offerIDValue); err != nil {
		ginerrors.Response(c, err)
		return
	}

	var req apartmentsmodels.CreateOfferRequest

	if err := c.ShouldBindJSON(&req); err != nil {
		ginerrors.Response(c, err)
		return
	}

	req.RealtorID = c.GetString("requester")
	msg := &apartmentsmodels.UpdateOfferRequest{
		Create:  &req,
		OfferID: uint64(offerID),
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)

	defer cancel()

	result, err := grpc.GetApartmentsClient().UpdateOffer(ctx, msg)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}

func (h ApartmentsHandler) DeleteOfferHandler(c *gin.Context) {
	offerIDValue := c.Param("offer_id")

	var (
		offerID int
		err     error
	)

	if offerID, err = strconv.Atoi(offerIDValue); err != nil {
		ginerrors.Response(c, err)
		return
	}

	req := &apartmentsmodels.DeleteOfferRequest{
		OwnerID: c.GetString("requester"),
		OfferID: uint64(offerID),
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)

	defer cancel()

	result, err := grpc.GetApartmentsClient().DeleteOffer(ctx, req)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}
