package handler

import (
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/sessions"
	"github.com/markbates/goth"
	"github.com/markbates/goth/providers/apple"
	"github.com/markbates/goth/providers/facebook"
	"github.com/markbates/goth/providers/github"
	"github.com/markbates/goth/providers/google"
	"github.com/markbates/goth/providers/linkedin"
	"github.com/markbates/goth/providers/twitter"
	ginerrors "github.com/microparts/errors-go-gin"
	"gitlab.com/apartstech/infrastructure.git/constant"
	"gitlab.com/apartstech/infrastructure.git/log"
	proto "gitlab.com/apartstech/proto.git/pkg/models/aclmodels"

	"gitlab.com/apartstech/layer2/auth.git/v1/cfg"
	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/grpc"
)

type key int

// providerParamKey can be used as a key in context when passing in a provider
const providerParamKey key = iota

type OAuth struct {
	// Store can/should be set by applications using gothic. The default is a cookie store.
	Store        sessions.Store
	defaultStore sessions.Store

	keySet bool
	key    []byte
}

type TempOAuth struct {
	Cancel func()
	Chan   chan interface{}
}

func OAuthLinks() (result map[string]string) {
	p := goth.GetProviders()

	result = make(map[string]string)

	for k := range p {
		result[k] = fmt.Sprintf("%s/api/v1/auth/o/%s/login", cfg.Store.WS.FullURL(), k)
	}

	return result
}

func NewOAuthController() *OAuth {
	// TODO
	os.Setenv("GOOGLE_KEY", "1023711387475-03onat8nmr3jtp8t3lpsvf6mbsjbtd98.apps.googleusercontent.com")
	os.Setenv("GOOGLE_SECRET", "I_SVUU48Z6JCdZ1jylrBC7L_")

	goth.UseProviders(
		twitter.New(os.Getenv("TWITTER_KEY"), os.Getenv("TWITTER_SECRET"), fmt.Sprintf("%s/api/v1/auth/o/twitter/callback", cfg.Store.WS.FullURL())),
		// If you'd like to use authenticate instead of authorize in Twitter provider, use this instead.
		// twitter.NewAuthenticate(os.Getenv("TWITTER_KEY"), os.Getenv("TWITTER_SECRET"), "http://localhost:3000/auth/twitter/callback"),

		facebook.New(os.Getenv("FACEBOOK_KEY"), os.Getenv("FACEBOOK_SECRET"), fmt.Sprintf("%s/api/v1/auth/o/facebook/callback", cfg.Store.WS.FullURL())),

		google.New(os.Getenv("GOOGLE_KEY"), os.Getenv("GOOGLE_SECRET"), fmt.Sprintf("%s/api/v1/auth/o/google/callback", cfg.Store.WS.FullURL())),

		linkedin.New(os.Getenv("LINKEDIN_KEY"), os.Getenv("LINKEDIN_SECRET"), fmt.Sprintf("%s/api/v1/auth/o/linkedin/callback", cfg.Store.WS.FullURL())),

		github.New(os.Getenv("GITHUB_KEY"), os.Getenv("GITHUB_SECRET"), fmt.Sprintf("%s/api/v1/auth/o/github/callback", cfg.Store.WS.FullURL())),

		apple.New(os.Getenv("APPLE_KEY"), os.Getenv("APPLE_SECRET"), fmt.Sprintf("%s/api/v1/auth/o/apple/callback", cfg.Store.WS.FullURL()), nil, apple.ScopeName, apple.ScopeEmail),
	)

	o := &OAuth{}

	o.key = []byte(cfg.Store.Auth.SessionSecret)
	o.keySet = len(o.key) != 0

	cookieStore := sessions.NewCookieStore(o.key)
	cookieStore.Options = &sessions.Options{
		MaxAge:   10 * 60,
		HttpOnly: true,
		Secure:   false,
	}

	cookieStore.Options.HttpOnly = true
	o.Store = cookieStore
	o.defaultStore = o.Store

	return o
}

func (o *OAuth) Login(ctx *gin.Context) {
	// try to get the user without re-authenticating
	if gothUser, err := o.completeUserAuth(ctx); err == nil {
		ctx.JSON(http.StatusOK, gothUser)
	} else {
		url, err := o.getAuthURL(ctx)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, err)
			return
		}

		ctx.Redirect(http.StatusTemporaryRedirect, url)
	}
}

func (o *OAuth) Logout(ctx *gin.Context) {
	// try to get the user without re-authenticating
	if err := o.logout(ctx); err != nil {
		ginerrors.Response(ctx, err)
	}

	ctx.Writer.Header().Set("Location", "/")
	ctx.Writer.WriteHeader(http.StatusTemporaryRedirect)
}

func (o *OAuth) Callback(ctx *gin.Context) {
	user, err := o.completeUserAuth(ctx)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, err)
		return
	}

	// TODO Login or Register then Redirect
	var randPassport string
	{
		rand.Seed(time.Now().UnixNano())
		chars := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
			"abcdefghijklmnopqrstuvwxyz" +
			"0123456789")
		length := 20
		var b strings.Builder
		for i := 0; i < length; i++ {
			b.WriteRune(chars[rand.Intn(len(chars))])
		}
		randPassport = b.String()
	}

	result, err := grpc.GetACLClient().UserRegister(ctx, &proto.UserRequest{User: &proto.User{Email: user.Email, Password: randPassport, Role: []string{constant.CasbinRoleMain}}})
	if err != nil {
		log.Error().Err(err).Send()
		ginerrors.Response(ctx, err)
		return
	}

	ctx.IndentedJSON(http.StatusOK, result)
}

// Deprecated: it's for testing, front
func (o *OAuth) Root(ctx *gin.Context) {
	var htmlIndex = fmt.Sprintf(`<html>
	<body>
		<a href="%v/api/v1/auth/o/google/login">Google Log In</a>
	</body>
</html>`, cfg.Store.WS.FullURL())

	ctx.Header("Content-Type", "text/html; charset=utf-8")
	ctx.String(http.StatusOK, htmlIndex)
}
