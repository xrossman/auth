package handler

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	ginerrors "github.com/microparts/errors-go-gin"
	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/grpc"
	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/validation"
	proto "gitlab.com/apartstech/proto.git/pkg/models/aclmodels"
)

func (c *User) AdminSetUserRole(ctx *gin.Context) {
	req, err := getUserRoleRequest(ctx)
	if err != nil {
		ginerrors.Response(ctx, err)
		return
	}

	if err := validation.Validate(req); err != nil {
		ginerrors.Response(ctx, err)
		return
	}

	ctxC, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	var result *proto.BoolReply

	result, err = grpc.GetACLClient().UserSetRole(ctxC, req)
	if err != nil {
		ginerrors.Response(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, result)
}

func (c *User) AdminDeleteUserRole(ctx *gin.Context) {
	req, err := getUserRoleRequest(ctx)
	if err != nil {
		ginerrors.Response(ctx, err)
		return
	}

	if err := validation.Validate(req); err != nil {
		ginerrors.Response(ctx, err)
		return
	}

	ctxC, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	var result *proto.BoolReply

	result, err = grpc.GetACLClient().UserDeleteRole(ctxC, req)
	if err != nil {
		ginerrors.Response(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, result)
}

func getUserRoleRequest(ctx *gin.Context) (*proto.UserRoleRequest, error) {
	idValue := ctx.Param("userID")
	guid, err := uuid.Parse(idValue)
	if err != nil {
		return nil, err
	}

	role := ctx.Param("role")
	if role == "" {
		return nil, fmt.Errorf("role can't be empty")
	}

	req := &proto.UserRoleRequest{UUID: guid.String(), Role: role}

	return req, nil
}
