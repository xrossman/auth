package handler

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	ginerrors "github.com/microparts/errors-go-gin"
	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/grpc"
	"gitlab.com/apartstech/proto.git/pkg/models/apartmentsmodels"
)

func (h ApartmentsHandler) CreateRoom(c *gin.Context) {
	apartIDValue := c.Param("apartment_id")

	var (
		err     error
		apartID int
	)

	if apartID, err = strconv.Atoi(apartIDValue); err != nil {
		ginerrors.Response(c, err)
		return
	}

	var req apartmentsmodels.CreateRoomRequest

	if err := c.ShouldBindJSON(&req); err != nil {
		ginerrors.Response(c, err)
		return
	}

	req.ApartmentID = uint64(apartID)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	result, err := grpc.GetApartmentsClient().CreateRoom(ctx, &req)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}

func (h ApartmentsHandler) ReadRoom(c *gin.Context) {
	apartIDValue := c.Param("apartment_id")

	var (
		err             error
		apartID, roomID int
	)

	if apartID, err = strconv.Atoi(apartIDValue); err != nil {
		ginerrors.Response(c, err)
		return
	}

	roomIDValue := c.Param("room_id")

	if roomID, err = strconv.Atoi(roomIDValue); err != nil {
		ginerrors.Response(c, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	result, err := grpc.GetApartmentsClient().ReadRoom(ctx, &apartmentsmodels.ReadRoomRequest{
		ApartmentID: uint64(apartID),
		RoomID:      uint64(roomID),
	})
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}
func (h ApartmentsHandler) ReadApartmentsRooms(c *gin.Context) {
	apartIDValue := c.Param("apartment_id")

	var (
		err     error
		apartID int
	)

	if apartID, err = strconv.Atoi(apartIDValue); err != nil {
		ginerrors.Response(c, err)
		return
	}

	req := &apartmentsmodels.ListApartmentRoomsRequest{
		ApartmentID: uint64(apartID),
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	result, err := grpc.GetApartmentsClient().ListApartmentRooms(ctx, req)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}
func (h ApartmentsHandler) UpdateRoom(c *gin.Context) {
	apartIDValue := c.Param("apartment_id")

	var (
		err             error
		roomID, apartID int
	)

	if apartID, err = strconv.Atoi(apartIDValue); err != nil {
		ginerrors.Response(c, err)
		return
	}

	roomIDValue := c.Param("room_id")

	if roomID, err = strconv.Atoi(roomIDValue); err != nil {
		ginerrors.Response(c, err)
		return
	}

	var req apartmentsmodels.UpdateRoomRequest

	if err := c.ShouldBindJSON(&req); err != nil {
		ginerrors.Response(c, err)
		return
	}

	req.ApartmentID = uint64(apartID)
	req.RoomID = uint64(roomID)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	result, err := grpc.GetApartmentsClient().UpdateRoom(ctx, &req)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}

func (h ApartmentsHandler) DeleteRoom(c *gin.Context) {
	apartIDValue := c.Param("apartment_id")

	var (
		err             error
		roomID, apartID int
	)

	if apartID, err = strconv.Atoi(apartIDValue); err != nil {
		ginerrors.Response(c, err)
		return
	}

	roomIDValue := c.Param("room_id")

	if roomID, err = strconv.Atoi(roomIDValue); err != nil {
		ginerrors.Response(c, err)
		return
	}

	req := &apartmentsmodels.DeleteRoomRequest{
		ApartmentID: uint64(apartID),
		RoomID:      uint64(roomID),
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	result, err := grpc.GetApartmentsClient().DeleteRoom(ctx, req)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}
