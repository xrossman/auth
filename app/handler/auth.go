package handler

import (
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	ginerrors "github.com/microparts/errors-go-gin"
	"gitlab.com/apartstech/infrastructure.git/log"
	"gitlab.com/apartstech/layer2/auth.git/v1/cfg"
	models "gitlab.com/apartstech/layer2/notifications.git/pkg/messagemodels"
	pub "gitlab.com/apartstech/layer2/notifications.git/pkg/publisher"
	proto "gitlab.com/apartstech/proto.git/pkg/models/aclmodels"

	"gitlab.com/apartstech/layer2/auth.git/v1/app/middleware"
	"gitlab.com/apartstech/layer2/auth.git/v1/domain/requests"
	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/grpc"
	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/validation"
)

type AuthHandlers struct {
}

// GetIP gets a requests IP address by reading off the forwarded-for
// header (for proxies) and falls back to use the remote address.
func (a *AuthHandlers) GetIP(r *http.Request) string {
	forwarded := r.Header.Get("X-FORWARDED-FOR")
	if forwarded != "" {
		return forwarded
	}
	return r.RemoteAddr
}

func (a *AuthHandlers) SignIn(ctx *gin.Context) {
	req := requests.LoginRequestMessage{}
	if err := ctx.ShouldBindJSON(&req); err != nil {
		log.Warn().Err(err).Send()
		ginerrors.Response(ctx, err)
		return
	}

	if err := validation.Validate(req); err != nil {
		log.Warn().Err(err).Send()
		ginerrors.Response(ctx, err)
		return
	}

	// Set up a connection to the server.
	ctxC, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	result, err := grpc.GetACLClient().SignIn(ctxC, &proto.LoginRequest{
		Login:    req.Login,
		Password: req.Password,
		Ip:       middleware.RealIP(ctx),
	})
	if err != nil {
		log.Error().Err(err).Send()
		ctx.JSON(http.StatusUnauthorized, err)
		return
	}

	ctx.JSON(http.StatusOK, result)
}

func (a *AuthHandlers) RenewToken(ctx *gin.Context) {
	req := requests.RenewTokenRequestMessage{}
	if err := ctx.ShouldBindJSON(&req); err != nil {
		log.Warn().Err(err).Send()
		ginerrors.Response(ctx, err)
		return
	}
	if err := validation.Validate(req); err != nil {
		log.Warn().Err(err).Send()
		ginerrors.Response(ctx, err)
		return
	}

	ctxC, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	result, err := grpc.GetACLClient().RenewToken(ctxC, &proto.UserAllowedRequest{
		Token: req.Token,
	})
	if err != nil {
		log.Error().Err(err).Send()
		ctx.JSON(http.StatusUnauthorized, err)
		return
	}

	ctx.JSON(http.StatusOK, result)
}

func (a *AuthHandlers) SignUpStepOne(ctx *gin.Context) {
	req := requests.SignUpStepOneRequestMessage{}
	if err := ctx.ShouldBindJSON(&req); err != nil {
		log.Warn().Err(err).Send()
		ginerrors.Response(ctx, err)
		return
	}

	if err := validation.Validate(req); err != nil {
		log.Warn().Err(err).Send()
		ginerrors.Response(ctx, err)
		return
	}

	ctxC, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	result, err := grpc.GetACLClient().UserRegister(ctxC, &proto.UserRequest{User: &proto.User{Email: req.Email, Password: req.Password}})
	if err != nil {
		log.Error().Err(err).Send()
		ginerrors.Response(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, result)
}

func (a *AuthHandlers) SigUpStepTwo(ctx *gin.Context) {
	code := ctx.Query("code")

	ctxC, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	result, err := grpc.GetACLClient().UserRegister(ctxC, &proto.UserRequest{
		User: &proto.User{Code: code},
		Ip:   middleware.RealIP(ctx),
	})
	if err != nil {
		log.Error().Err(err).Send()
		ginerrors.Response(ctx, err)
		return
	}

	message := models.NewRegistrationConfirmationEmail(cfg.Store.WS.PublicURL, cfg.Store.WS.ServiceName, result.User.Email)

	if err := pub.AddRegistrationConfirmationMessage(message); err != nil {
		log.Error().Err(err).Send()
		ginerrors.Response(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, result)
}

func (a AuthHandlers) SubscribeHandler(ctx *gin.Context) {
	var req requests.SubscribeRequestMessage

	if err := ctx.ShouldBindJSON(&req); err != nil {
		log.Error().Err(err).Send()
		ginerrors.Response(ctx, err)
		return
	}

	if err := validation.Validate(req); err != nil {
		log.Warn().Err(err).Send()
		ginerrors.Response(ctx, err)
		return
	}

	ctxC, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	result, err := grpc.GetACLClient().Subscribe(ctxC, &proto.SubscribeRequest{
		Email: req.Email,
	})
	if err != nil {
		log.Error().Err(err).Send()
		ginerrors.Response(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, result)
}

func (a AuthHandlers) RoleList(ctx *gin.Context) {
	ctxC, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	result, err := grpc.GetACLClient().RoleList(ctxC, &proto.EmptyRequest{})
	if err != nil {
		log.Error().Err(err).Send()
		ginerrors.Response(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, result)
}

func (a AuthHandlers) RoleCreate(ctx *gin.Context) {
	var req proto.Role

	if err := ctx.ShouldBindJSON(&req); err != nil {
		log.Warn().Err(err).Send()
		ginerrors.Response(ctx, err)
		return
	}

	ctxC, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	result, err := grpc.GetACLClient().RoleCreate(ctxC, &proto.RoleRequest{Role: &req})
	if err != nil {
		log.Error().Err(err).Send()
		ginerrors.Response(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, result)
}

func (a AuthHandlers) RoleMarkDelete(ctx *gin.Context) {
	var req proto.Role

	if err := ctx.ShouldBindJSON(&req); err != nil {
		log.Warn().Err(err).Send()
		ginerrors.Response(ctx, err)
		return
	}

	if err := validation.V.Var(req.ID, "required"); err != nil {
		log.Warn().Err(err).Send()
		ginerrors.Response(ctx, err)
		return
	}

	ctxC, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	result, err := grpc.GetACLClient().RoleDelete(ctxC, &proto.RoleRequest{Role: &req})
	if err != nil {
		log.Error().Err(err).Send()
		ginerrors.Response(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, result)
}
