package handler

import (
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang/protobuf/ptypes/empty"
	ginerrors "github.com/microparts/errors-go-gin"
	"gitlab.com/apartstech/infrastructure.git/log"
	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/grpc"
	"gitlab.com/apartstech/proto.git/pkg/models"
	"gitlab.com/apartstech/proto.git/pkg/models/apartmentsmodels"
)

func (h ApartmentsHandler) PropertyRanksAndTypes(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, h.typesAndRanks)
}

func (h ApartmentsHandler) ListAmenities(c *gin.Context) {
	grouped := c.Query("grouped_amenities")

	p, err := models.NewPaginationFromGinContext(c)
	if err != nil {
		log.Error().Err(err).Msgf("[LIST AMENITIES] can't bind pagination: %v", err)
		ginerrors.Response(c, err)
		return
	}

	var (
		result interface{}
	)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	switch grouped {
	case "":
		req := &apartmentsmodels.ListAmenitiesRequest{
			Pagination: p,
		}
		result, err = grpc.GetApartmentsClient().ListAmenities(ctx, req)
	default:
		req := &empty.Empty{}
		result, err = grpc.GetApartmentsClient().ListGroupedAmenities(ctx, req)
	}

	if err != nil {
		log.Error().Err(err).Msgf("[LIST AMENITIES] can't list amenities: %v", err)
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}
