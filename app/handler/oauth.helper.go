package handler

import (
	"bytes"
	"compress/gzip"
	"context"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/sessions"
	"github.com/markbates/goth"

	"gitlab.com/apartstech/layer2/auth.git/v1/common"
)

// setState sets the state string associated with the given request.
// If no state string is associated with the request, one will be generated.
// This state is sent to the provider and can be retrieved during the
// callback.
var setState = func(req *http.Request) string {
	state := req.URL.Query().Get("state")
	if len(state) > 0 {
		return state
	}

	// If a state query param is not passed in, generate a random
	// base64-encoded nonce so that the state on the auth URL
	// is unguessable, preventing CSRF attacks, as described in
	//
	// https://auth0.com/docs/protocols/oauth2/oauth-state#keep-reading
	nonceBytes := make([]byte, 64)
	_, err := io.ReadFull(rand.Reader, nonceBytes)
	if err != nil {
		panic("source of randomness unavailable: " + err.Error())
	}
	return base64.URLEncoding.EncodeToString(nonceBytes)
}

// getState gets the state returned by the provider during the callback.
// This is used to prevent CSRF attacks, see
// http://tools.ietf.org/html/rfc6749#section-10.12
var getState = func(ctx *gin.Context) string {
	return ctx.Request.URL.Query().Get("state")
}

/*
getAuthURL starts the authentication process with the requested provided.
It will return a URL that should be used to send users to.

It expects to be able to get the name of the provider from the query parameters
as either "provider" or ":provider".

I would recommend using the BeginAuthHandler instead of doing all of these steps
yourself, but that's entirely up to you.
*/
func (o *OAuth) getAuthURL(ctx *gin.Context) (string, error) {
	if !o.keySet && o.defaultStore == o.Store {
		fmt.Println("no SESSION_SECRET environment variable is set. The default cookie store is not available and any calls will fail. Ignore this warning if you are using a different store.")
	}

	providerName := ctx.Param("provider")

	if providerName == "" {
		// if not found then return an empty string with the corresponding error
		return "", errors.New("you must select a provider")
	}
	// As a fallback, loop over the used providers, if we already have a valid session for any provider (ie. user has already begun authentication with a provider), then return that provider name
	providers := goth.GetProviders()
	session, _ := o.Store.Get(ctx.Request, common.SessionNameOauth)

	for _, provider := range providers {
		p := provider.Name()
		value := session.Values[p]
		if _, ok := value.(string); ok {
			return p, nil
		}
	}

	provider, err := goth.GetProvider(providerName)
	if err != nil {
		return "", err
	}

	sess, err := provider.BeginAuth(setState(ctx.Request))
	if err != nil {
		return "", err
	}

	u, err := sess.GetAuthURL()
	if err != nil {
		return "", err
	}

	err = o.storeInSession(providerName, sess.Marshal(), ctx)
	if err != nil {
		return "", err
	}

	return u, err
}

/*
CompleteUserAuth does what it says on the tin. It completes the authentication
process and fetches all of the basic information about the user from the provider.

It expects to be able to get the name of the provider from the query parameters
as either "provider" or ":provider".

See https://github.com/markbates/goth/examples/main.go to see this in action.
*/
func (o *OAuth) completeUserAuth(ctx *gin.Context) (goth.User, error) {
	defer func() {
		if err := o.logout(ctx); err != nil {
			log.Print(err)
		}
	}()

	if !o.keySet && o.defaultStore == o.Store {
		fmt.Println("goth/gothic: no SESSION_SECRET environment variable is set. The default cookie store is not available and any calls will fail. Ignore this warning if you are using a different store.")
	}

	providerName := ctx.Param("provider")

	if providerName == "" {
		// if not found then return an empty string with the corresponding error
		return goth.User{}, errors.New("you must select a provider")
	}

	provider, err := goth.GetProvider(providerName)
	if err != nil {
		return goth.User{}, err
	}

	value, err := o.getFromSession(providerName, ctx.Request)
	if err != nil {
		return goth.User{}, err
	}

	sess, err := provider.UnmarshalSession(value)
	if err != nil {
		return goth.User{}, err
	}

	err = o.validateState(ctx.Request, sess)
	if err != nil {
		return goth.User{}, err
	}

	user, err := provider.FetchUser(sess)
	if err == nil {
		// user can be found with existing session data
		return user, err
	}

	// get new token and retry fetch
	_, err = sess.Authorize(provider, ctx.Request.URL.Query())
	if err != nil {
		return goth.User{}, err
	}

	err = o.storeInSession(providerName, sess.Marshal(), ctx)

	if err != nil {
		return goth.User{}, err
	}

	gu, err := provider.FetchUser(sess)
	return gu, err
}

// validateState ensures that the state token param from the original
// AuthURL matches the one included in the current (callback) request.
func (o *OAuth) validateState(req *http.Request, sess goth.Session) error {
	rawAuthURL, err := sess.GetAuthURL()
	if err != nil {
		return err
	}

	authURL, err := url.Parse(rawAuthURL)
	if err != nil {
		return err
	}

	originalState := authURL.Query().Get("state")
	if originalState != "" && (originalState != req.URL.Query().Get("state")) {
		return errors.New("state token mismatch")
	}
	return nil
}

// logout invalidates a user session.
func (o *OAuth) logout(ctx *gin.Context) error {
	session, err := o.Store.Get(ctx.Request, common.SessionNameOauth)
	if err != nil {
		return err
	}
	session.Options.MaxAge = -1
	session.Values = make(map[interface{}]interface{})
	err = session.Save(ctx.Request, ctx.Writer)
	if err != nil {
		return errors.New("could not delete user session ")
	}
	return nil
}

// getContextWithProvider returns a new request context containing the provider
func (o *OAuth) getContextWithProvider(req *http.Request, provider string) *http.Request {
	return req.WithContext(context.WithValue(req.Context(), providerParamKey, provider))
}

// storeInSession stores a specified key/value pair in the session.
func (o *OAuth) storeInSession(key string, value string, ctx *gin.Context) error {
	session, _ := o.Store.New(ctx.Request, common.SessionNameOauth)

	if err := o.updateSessionValue(session, key, value); err != nil {
		return err
	}

	return session.Save(ctx.Request, ctx.Writer)
}

// getFromSession retrieves a previously-stored value from the session.
// If no value has previously been stored at the specified key, it will return an error.
func (o *OAuth) getFromSession(key string, req *http.Request) (string, error) {
	session, _ := o.Store.Get(req, common.SessionNameOauth)
	value, err := o.getSessionValue(session, key)
	if err != nil {
		return "", errors.New("could not find a matching session for this request")
	}

	return value, nil
}

func (o *OAuth) getSessionValue(session *sessions.Session, key string) (string, error) {
	value := session.Values[key]
	if value == nil {
		return "", fmt.Errorf("could not find a matching session for this request")
	}

	rdata := strings.NewReader(value.(string))
	r, err := gzip.NewReader(rdata)
	if err != nil {
		return "", err
	}
	s, err := ioutil.ReadAll(r)
	if err != nil {
		return "", err
	}

	return string(s), nil
}

func (o *OAuth) updateSessionValue(session *sessions.Session, key, value string) error {
	var b bytes.Buffer
	gz := gzip.NewWriter(&b)
	if _, err := gz.Write([]byte(value)); err != nil {
		return err
	}
	if err := gz.Flush(); err != nil {
		return err
	}
	if err := gz.Close(); err != nil {
		return err
	}

	session.Values[key] = b.String()
	return nil
}
