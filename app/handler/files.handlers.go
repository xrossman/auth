package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	ginerrors "github.com/microparts/errors-go-gin"
	"gitlab.com/apartstech/layer2/auth.git/v1/domain/commands"
)

type FilesHandlers struct{}

func (h FilesHandlers) AddImage(c *gin.Context) {
	category := c.Param("category")

	data, err := c.GetRawData()
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	name, err := commands.SaveImage(data, category)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{"name": name})
}

func (h FilesHandlers) DeleteImage(c *gin.Context) {
	category := c.Param("category")
	name := c.Param("image_name")

	err := commands.DeleteImage(category, name)
	if err != nil {
		ginerrors.Response(c, err)
		return
	}

	c.Status(http.StatusOK)
}
