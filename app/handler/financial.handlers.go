package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	ginerrors "github.com/microparts/errors-go-gin"
	"gitlab.com/apartstech/infrastructure.git/log"
	"gitlab.com/apartstech/layer2/auth.git/v1/domain/commands"
	"gitlab.com/apartstech/proto.git/pkg/models/financialmodels"
)

type FinancialHandler struct{}

func (h FinancialHandler) ConvertCurrencyHandler(ctx *gin.Context) {
	var req financialmodels.ConvertSingleCurrencyRequest

	if err := ctx.ShouldBindJSON(&req); err != nil {
		log.Error().Msgf("[CONVERT CURRENCY] can't bind request: %v", err)
		ginerrors.Response(ctx, err)
		return
	}

	res, err := commands.ConvertSingleCurrencyCommand(&req)
	if err != nil {
		log.Error().Msgf("[CONVERT CURRENCY] can't convert currency: %v", err)
		ginerrors.Response(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, res)
}
