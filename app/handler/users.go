package handler

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	ginerrors "github.com/microparts/errors-go-gin"
	"gitlab.com/apartstech/infrastructure.git/log"
	"gitlab.com/apartstech/layer2/auth.git/v1/app/middleware"
	"gitlab.com/apartstech/layer2/auth.git/v1/domain/commands"
	cmnproto "gitlab.com/apartstech/proto.git/pkg/models"
	proto "gitlab.com/apartstech/proto.git/pkg/models/aclmodels"

	"gitlab.com/apartstech/layer2/auth.git/v1/cfg"
	"gitlab.com/apartstech/layer2/auth.git/v1/domain/requests"
	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/grpc"
	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/validation"
)

type User struct {
}

func (c *User) ReadRealtor(ctx *gin.Context) {
	name := ctx.Param("display_name")

	user, err := grpc.GetACLClient().User(ctx, &proto.UserRequest{
		DisplayName: name,
	})
	if err != nil {
		ginerrors.Response(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, user)
	return
}

func (c *User) Read(ctx *gin.Context) {
	idValue := ctx.Param("userID")
	if idValue == "" {
		loggedUser, b := ctx.Get("logged_user")
		if !b {
			ctx.JSON(http.StatusBadRequest, "login please")
			return
		}

		u, _ := loggedUser.(*proto.User)

		ctx.JSON(http.StatusOK, &proto.UserResponse{User: u})

		return
	}

	uid, err := uuid.Parse(idValue)
	if err != nil {
		ginerrors.Response(ctx, err)
		return
	}

	ctxC, cancel := context.WithTimeout(context.Background(), time.Second)

	defer cancel()

	result, err := grpc.GetACLClient().User(ctxC, &proto.UserRequest{
		UUID: uid.String(),
	})

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, result)
}

func (c *User) Search(ctx *gin.Context) {
	guid, err := uuid.Parse(ctx.Param("userID"))
	if err != nil {
		ginerrors.Response(ctx, err)
		return
	}

	userLogin := ctx.Param("login")
	userDName := ctx.Param("display_name")

	ctxC, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	result, err := grpc.GetACLClient().User(ctxC, &proto.UserRequest{
		UUID:        guid.String(),
		Login:       userLogin,
		DisplayName: userDName,
	})

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, result)
}

func (c *User) AdminSearch(ctx *gin.Context) {
	p, err := cmnproto.NewOffsetLimitPaginationFromGinContext(ctx)
	if err != nil {
		ginerrors.Response(ctx, err)
	}

	userRole := ctx.Param("role")
	userDeleted, _ := strconv.ParseBool(ctx.Param("deleted"))
	userSearchStr := ctx.Param("search")

	ctxC, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	result, err := grpc.GetACLClient().UserSearch(ctxC, &proto.UserSearchRequest{
		String_:    userSearchStr,
		Role:       userRole,
		Deleted:    userDeleted,
		Pagination: p,
	})

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, result)
}

func (c *User) Create(ctx *gin.Context) {
	req := &proto.UserRequest{}
	if err := ctx.ShouldBindJSON(req); err != nil {
		ginerrors.Response(ctx, err)
		return
	}

	if err := validation.Validate(req); err != nil {
		ginerrors.Response(ctx, err)
		return
	}

	ctxC, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	result, err := grpc.GetACLClient().UserCreate(ctxC, req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, result)
}

func (c *User) Delete(ctx *gin.Context) {
	var IDList []string

	if ctx.FullPath() == fmt.Sprintf("/api/%s/%s/", cfg.Store.WS.ServiceVersion, "profile") {
		// update profile
		id, b := ctx.Get("logged_user")
		if !b {
			ctx.JSON(http.StatusBadRequest, "login please")
			return
		}

		var (
			u  *proto.User
			ok bool
		)
		// Set UUID
		if u, ok = id.(*proto.User); !ok {
			ctx.JSON(http.StatusBadRequest, "login please.")
			return
		}
		IDList = []string{u.UUID}
	} else if err := ctx.ShouldBindJSON(&IDList); err != nil {
		ginerrors.Response(ctx, err)
		return
	}

	ctxC, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	result, err := grpc.GetACLClient().UserDelete(ctxC, &proto.UserListRequest{
		UserFilter: proto.UserFilter{IDList: IDList},
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, result)
}

func (c *User) Update(ctx *gin.Context) {
	req := requests.UpdateUserRequestMessage{}
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ginerrors.Response(ctx, err)
		return
	}
	idValue := ctx.Param("userID")
	if idValue == "" {
		// update profile
		id, b := ctx.Get("logged_user")
		if !b {
			ctx.JSON(http.StatusBadRequest, "login please")
			return
		}

		var (
			u  *proto.User
			ok bool
		)
		// Set UUID
		if u, ok = id.(*proto.User); !ok {
			ctx.JSON(http.StatusBadRequest, "login please.")
			return
		}
		guid, err := uuid.Parse(u.UUID)
		if err != nil {
			log.Error().Err(err).Msgf("uuid string: '%v'", idValue)
			ginerrors.Response(ctx, err)

			return
		}
		req.UUID = guid
	} else {
		// Update different uuid
		guid, err := uuid.Parse(idValue)
		if err != nil {
			log.Error().Err(err).Msgf("uuid string: '%v'", idValue)
			ginerrors.Response(ctx, err)

			return
		}

		req.UUID = guid
	}

	if err := validation.Validate(req); err != nil {
		ginerrors.Response(ctx, err)
		return
	}

	ctxC, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	result, err := grpc.GetACLClient().UserUpdate(ctxC, &proto.UserRequest{UUID: req.UUID.String(), User: req.User})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, result)
}

func (c *User) List(ctx *gin.Context) {
	p, err := cmnproto.NewOffsetLimitPaginationFromGinContext(ctx)
	if err != nil {
		ginerrors.Response(ctx, err)
		return
	}

	q := ctx.Query("q")

	ctxC, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	result, err := grpc.GetACLClient().UserSearch(ctxC, &proto.UserSearchRequest{
		String_:    q,
		Pagination: p,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, result)
}

func (c *User) PasswordUpdate(ctx *gin.Context) {
	// update profile
	id, b := ctx.Get("logged_user")
	if !b {
		ctx.JSON(http.StatusBadRequest, "login please")
		return
	}

	var (
		u  *proto.User
		ok bool
	)
	// Set UUID
	if u, ok = id.(*proto.User); !ok {
		ctx.JSON(http.StatusBadRequest, "login please.")
		return
	}

	req, err := getChangePwdRequestFromContext(ctx, u.UUID)
	if err != nil {
		ginerrors.Response(ctx, err)
		return
	}

	if req.OldPassword == "" || req.NewPassword == "" {
		ginerrors.Response(ctx, fmt.Errorf("password is empty"))
		return
	}

	if err := validation.Validate(req); err != nil {
		ginerrors.Response(ctx, err)
		return
	}

	ctxC, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	result, err := grpc.GetACLClient().UserChangePassword(ctxC, req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, result)
}

func (c *User) AdminResetUserPassword(ctx *gin.Context) {
	idValue := ctx.Param("userID")

	req, err := getChangePwdRequestFromContext(ctx, idValue)
	if err != nil {
		ginerrors.Response(ctx, err)
		return
	}

	if err := validation.Validate(req); err != nil {
		ginerrors.Response(ctx, err)
		return
	}

	req.Force = true

	ctxC, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	result, err := grpc.GetACLClient().UserChangePassword(ctxC, req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, result)
}

func (c *User) SetAvatar(ctx *gin.Context) {
	user, err := middleware.GetLoggedUserFromContext(ctx)
	if err != nil {
		ginerrors.Response(ctx, err)
		return
	}

	data, err := ctx.GetRawData()
	if err != nil {
		ginerrors.Response(ctx, err)
		return
	}

	name, err := commands.SetAvatar(data, user)
	if err != nil {
		ginerrors.Response(ctx, err)
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"name": name})
}

func getChangePwdRequestFromContext(ctx *gin.Context, guid string) (*proto.UserChangePassword, error) {
	req := proto.UserChangePassword{}
	if err := ctx.ShouldBindJSON(&req); err != nil {
		return nil, err
	}

	req.UUID = guid

	return &req, nil
}
