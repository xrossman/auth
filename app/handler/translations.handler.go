package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	ginerrors "github.com/microparts/errors-go-gin"
	"gitlab.com/apartstech/infrastructure.git/log"
	"gitlab.com/apartstech/layer2/auth.git/v1/domain/commands"
	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/translations"
)

type TranslationsHandler struct{}

func (h *TranslationsHandler) TranslateText(ctx *gin.Context) {
	req := translations.TranslateRequest{}

	if err := ctx.ShouldBindJSON(&req); err != nil {
		log.Error().Msgf("[TRANSLATE TEXT] can't bind request: %v", err)
		ginerrors.Response(ctx, err)
		return
	}

	result, err := commands.TranslateTextCommand(req.TargetLang, req.Text)
	if err != nil {
		log.Error().Msgf("[TRANSLATE TEXT] can't translate text: %v", err)
		ginerrors.Response(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, translations.TranslateResponse{Result: result})
}
