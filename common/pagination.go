package common

import (
	"strconv"

	"github.com/gin-gonic/gin"
)

type Pagination struct {
	Total      int    `json:"total"`
	TotalPages int    `json:"total_pages"`
	Page       int    `json:"page"`
	PerPage    int    `json:"per_page"`
	NextPage   int    `json:"next_page"`
	PrevPage   int    `json:"prev_page"`
	Links      *Links `json:"links"`
}

type Links struct {
	Next string `json:"next"`
	Prev string `json:"prev"`
}

func (m *Pagination) SetTotal(total int) {
	m.Total = total
}

func (m *Pagination) SetPage() {}

func (m *Pagination) Refresh() {
	m.TotalPages = m.Total / m.PerPage
	if m.Total%m.PerPage > 0 {
		m.TotalPages++
	}

	if m.Page < m.TotalPages && m.Page > 0 {
		m.NextPage = m.Page + 1
		m.Page++
	}
	if m.Page > 0 {
		m.PrevPage = m.Page - 1
	}
}

func (m *Pagination) GetOffset() int {
	if m != nil {
		if m.Page > 1 {
			return (m.Page - 1) * m.PerPage
		}
	}
	return 0
}

func NewPaginationFromGinContext(c *gin.Context) Pagination {
	perPage, _ := strconv.Atoi(c.Query("per_page"))
	page, _ := strconv.Atoi(c.Query("page"))

	if perPage == 0 {
		perPage = 50
	}
	if page == 0 {
		page = 1
	}
	return Pagination{
		PerPage: perPage,
		Page:    page,
	}
}
