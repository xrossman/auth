package cfg

import (
	"encoding/base64"
	"fmt"
	"os"
	"time"

	"github.com/joho/godotenv"
	config "github.com/spacetab-io/configuration-go"
	rbcfg "gitlab.com/apartstech/layer2/notifications.git/cfg"
	rpcConfig "gitlab.com/apartstech/proto.git/pkg/config"
	"gopkg.in/yaml.v2"
)

type configStore struct {
	TranslateServiceKey []byte `env:"SERVICE_KEY" required:"true"`
	RemoteGRPC          struct {
		Apartments rpcConfig.GRPCConfig `yaml:"apartments"`
		ACL        rpcConfig.GRPCConfig `yaml:"acl"`
		Maps       rpcConfig.GRPCConfig `yaml:"maps"`
		Files      rpcConfig.GRPCConfig `yaml:"files"`
		Finance    rpcConfig.GRPCConfig `yaml:"fin"`
	} `yaml:"remote_grpc"`
	MessageQueue       rbcfg.RabbitCfg    `yaml:"message_queue"`
	ServiceDescription serviceDescription `yaml:"service_desc"`
	Auth               authCfg            `yaml:"auth"`
	WS                 serviceCfg         `yaml:"ws"`
	DB                 databaseCfg        `yaml:"db"`
	// Mail               mailCfg            `yaml:"mail"` // Deprecated
	ProxyTargets proxyTargets `yaml:"proxy_targets"`
}

type serviceDescription struct {
	ServiceAlias   string `json:"service_alias" yaml:"service_alias"`
	ServiceName    string `json:"service_name" yaml:"service_name"`
	ServiceVersion string `json:"service_version" yaml:"service_version"`
	Stage          string `json:"stage" yaml:"stage"`
}

type serviceCfg struct {
	Schema         string        `yaml:"schema"`
	Host           string        `yaml:"host"`
	Port           uint          `yaml:"port"`
	PublicURL      string        `yaml:"public_url"`
	ServiceName    string        `yaml:"service_name"`
	ServiceVersion string        `yaml:"service_version"`
	ContextTimeout time.Duration `yaml:"context_timeout"`
}

func (s serviceCfg) URL() string {
	if s.Port != 0 {
		return fmt.Sprintf("%s:%d", s.Host, s.Port)
	}
	return s.Host
}

func (s serviceCfg) FullURL() string {
	if s.Port != 0 {
		return fmt.Sprintf("%s://%s:%d", s.Schema, s.Host, s.Port)
	}
	return s.Host
}

type authCfg struct {
	TokenSecret      string `yaml:"token_secret"`
	RenewTokenSecret string `yaml:"renew_token_secret"`
	SessionSecret    string `yaml:"session_secret"`
}

type proxyTargets struct {
	Grafana string `yaml:"grafana"`
	Jaeger  string `yaml:"jaeger"`
}

type databaseCfg struct {
	Host          string `yaml:"host"`
	Port          uint   `yaml:"port"`
	User          string `yaml:"user"`
	Pass          string `yaml:"pass"`
	Name          string `yaml:"name"`
	SSLMode       string `yaml:"ssl_mode"`
	DSN           string
	MigrationDSN  string
	DefaultSchema string `yaml:"schema"`
}

var Store *configStore

func Init() error {
	godotenv.Load()

	path := config.GetEnv("CONFIG_PATH", "")
	cfgBytes, err := config.ReadConfigs(path)
	if err != nil {
		return fmt.Errorf("can't read config files: %s", err)
	}
	err = yaml.Unmarshal(cfgBytes, &Store)
	if err != nil {
		return fmt.Errorf("can't unmarshal yaml: %s", err)
	}
	Store.DB.DSN = fmt.Sprintf(
		"postgres://%s:%s@%s:%d/%s?sslmode=%s",
		Store.DB.User,
		Store.DB.Pass,
		Store.DB.Host,
		Store.DB.Port,
		Store.DB.Name,
		Store.DB.SSLMode,
	)
	Store.DB.MigrationDSN = fmt.Sprintf("%s&x-migrations-table=%s.schema_migrations", Store.DB.DSN, Store.DB.DefaultSchema)

	if serviceName, ok := os.LookupEnv("SERVICE_NAME"); ok {
		Store.ServiceDescription.ServiceName = serviceName
	}

	if version, ok := os.LookupEnv("SERVICE_VERSION"); ok {
		Store.ServiceDescription.ServiceVersion = version
	}

	serviceKey, ok := os.LookupEnv("SERVICE_KEY")
	if !ok {
		return fmt.Errorf("can't lookup SERVICE_KEY env")
	}

	if stage, ok := os.LookupEnv("STAGE"); ok {
		Store.ServiceDescription.Stage = stage
	}

	decodedKey, err := base64.StdEncoding.DecodeString(serviceKey)
	if err != nil {
		return err
	}

	Store.TranslateServiceKey = decodedKey

	return nil
}
