IMAGE=fair-layer2-auth
TAG=latest
SSH_PATH:=~/.ssh/id_rsa
SERVICE_OUT="./bin/service"
DATABASE_URL := ./bin/dbUrl
MIGRATE_VERSION = v4.3.0
MIGRATE_PLATFORM = linux
PKG="gitlab.com/apartstech/layer2/auth.git/v1"
SERVICE_PKG_BUILD="./cmd/service"
PKG_LIST=$(shell go list ${PKG}/... | grep -v /vendor/)
.PHONY: all
all: service

dep: ## Get the dependencies
	@go get -v -d ./...

service: ## Build the binary file for server
	@go build -mod=vendor -o $(SERVICE_OUT) $(SERVICE_PKG_BUILD)

image: ## Builds docker image
	docker build -t $(IMAGE):$(TAG) --build-arg SSH_PRIVATE_KEY="$$(cat ${SSH_PATH})" .
clean: ## Remove previous builds
	@rm $(SERVER_OUT) $(CLIENT_OUT) $(API_OUT) $(API_REST_OUT)
help: ## Display this help screen
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: lint
lint:
	@golangci-lint run ./...
