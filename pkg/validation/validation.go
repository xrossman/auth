package validation

import "gopkg.in/go-playground/validator.v9"

var V *validator.Validate

func Init() {
	V = validator.New()
}

func Validate(m interface{}) error {
	return V.Struct(m)
}
