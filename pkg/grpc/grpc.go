package grpc

import (
	"gitlab.com/apartstech/infrastructure.git/log"
	"gitlab.com/apartstech/proto.git/pkg/helpers"
	"gitlab.com/apartstech/proto.git/pkg/services/aclservice"
	"gitlab.com/apartstech/proto.git/pkg/services/apartments"
	"gitlab.com/apartstech/proto.git/pkg/services/files"
	"gitlab.com/apartstech/proto.git/pkg/services/financial"
	"gitlab.com/apartstech/proto.git/pkg/services/mapsagent"
	"google.golang.org/grpc"

	"gitlab.com/apartstech/layer2/auth.git/v1/cfg"
)

var (
	aclConn, mapsConn, apartConn, filesConn, finConn *grpc.ClientConn
)

func Init() error {
	var (
		err error
	)

	if cfg.Store.RemoteGRPC.ACL.Enabled {
		aclConn, err = helpers.GetGRPClientConn(cfg.Store.RemoteGRPC.ACL)
		if err != nil {
			return err
		}

		go helpers.CheckConnectionState(aclConn, cfg.Store.RemoteGRPC.ACL)
	}

	if cfg.Store.RemoteGRPC.Apartments.Enabled {
		apartConn, err = helpers.GetGRPClientConn(cfg.Store.RemoteGRPC.Apartments)
		if err != nil {
			return err
		}

		go helpers.CheckConnectionState(apartConn, cfg.Store.RemoteGRPC.Apartments)
	}

	if cfg.Store.RemoteGRPC.Maps.Enabled {
		mapsConn, err = helpers.GetGRPClientConn(cfg.Store.RemoteGRPC.Maps)
		if err != nil {
			return err
		}

		go helpers.CheckConnectionState(mapsConn, cfg.Store.RemoteGRPC.Maps)
	}

	if cfg.Store.RemoteGRPC.Files.Enabled {
		filesConn, err = helpers.GetGRPClientConn(cfg.Store.RemoteGRPC.Files)
		if err != nil {
			return err
		}

		go helpers.CheckConnectionState(filesConn, cfg.Store.RemoteGRPC.Files)
	}

	if cfg.Store.RemoteGRPC.Finance.Enabled {
		finConn, err = helpers.GetGRPClientConn(cfg.Store.RemoteGRPC.Finance)
		if err != nil {
			return err
		}

		go helpers.CheckConnectionState(finConn, cfg.Store.RemoteGRPC.Finance)
	}

	return nil
}

func CloseConnections() {
	if err := aclConn.Close(); err != nil {
		log.Err(err).Send()
	}

	if err := mapsConn.Close(); err != nil {
		log.Err(err).Send()
	}

	if err := apartConn.Close(); err != nil {
		log.Err(err).Send()
	}

	if err := filesConn.Close(); err != nil {
		log.Err(err).Send()
	}

	if err := finConn.Close(); err != nil {
		log.Err(err).Send()
	}
}

func GetACLClient() aclservice.AclClient {
	return aclservice.NewAclClient(aclConn)
}

func GetApartmentsClient() apartments.ApartmentsClient {
	return apartments.NewApartmentsClient(apartConn)
}

func GetMapsClient() mapsagent.PlacesClient {
	return mapsagent.NewPlacesClient(mapsConn)
}

func GetFilesClient() files.FilesClient {
	return files.NewFilesClient(filesConn)
}

func GetFinanceClient() financial.FinancialServiceClient {
	return financial.NewFinancialServiceClient(finConn)
}
