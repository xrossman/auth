package translations

func DetectMissingLanguages(source map[string]string) []string {
	result := make([]string, 0)
	for l, _ := range availableLanguages {
		if _, ok := source[l]; !ok {
			result = append(result, l)
		}
	}

	return result
}
