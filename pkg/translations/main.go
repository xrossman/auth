package translations

import (
	"context"
	"fmt"

	tr "cloud.google.com/go/translate"
	"gitlab.com/apartstech/layer2/auth.git/v1/cfg"
	"golang.org/x/text/language"
	"google.golang.org/api/option"
)

const (
	englishLang = "en"
	spanishLang = "es"
)

var availableLanguages = map[string]struct{}{
	englishLang: {},
	spanishLang: {},
}

func Translate(targetLang, text string) (string, error) {
	ctx := context.Background()

	lang, err := language.Parse(targetLang)
	if err != nil {
		return "", fmt.Errorf("can't parse language value %s: %v", targetLang, err)
	}

	client, err := tr.NewClient(ctx, option.WithCredentialsJSON(cfg.Store.TranslateServiceKey))
	if err != nil {
		return "", err
	}

	defer client.Close()

	resp, err := client.Translate(ctx, []string{text}, lang, nil)
	if err != nil {
		return "", fmt.Errorf("can't translate text: %v", err)
	}
	if len(resp) == 0 {
		return "", fmt.Errorf("translate returned empty response to text: %s", text)
	}
	return resp[0].Text, nil
}
