package translations

type TranslateRequest struct {
	TargetLang string `json:"target_lang"`
	Text       string `json:"text"`
}

type TranslateResponse struct {
	Result string `json:"result"`
}
