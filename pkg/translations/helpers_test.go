package translations

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDetectMissingLanguages(t *testing.T) {
	type testCase struct {
		name     string
		input    map[string]string
		expected []string
	}

	var cases = []testCase{
		{
			name:     "zero expected",
			input:    map[string]string{englishLang: "foo", spanishLang: "bar"},
			expected: []string{},
		},
		{
			name:     "one expected",
			input:    map[string]string{englishLang: "foo"},
			expected: []string{spanishLang},
		},
		{
			name:     "two expected",
			input:    map[string]string{},
			expected: []string{englishLang, spanishLang},
		},
	}

	runTest := func(cc testCase) func(t *testing.T) {
		return func(t *testing.T) {
			result := DetectMissingLanguages(cc.input)

			assert.Equal(t, cc.expected, result)
		}
	}

	for _, cc := range cases {
		t.Run(cc.name, runTest(cc))
	}
}
