ARG GO_VERSION=1.16

FROM golang:${GO_VERSION} as build-env

RUN apt-get -y update && apt-get -y install openssl git make curl gettext

ARG GOPRIVATE="gitlab.com/apartstech/*"

ARG GITLAB_TOKEN
RUN git config --global url."https://gitlab-ci-token:${GITLAB_TOKEN}@gitlab.com/".insteadOf 'https://gitlab.com/'

ENV CGO_ENABLED=0

ADD . /build
WORKDIR /build

RUN make service

FROM golang:1.16-alpine
RUN apk update && apk add --no-cache \
        ca-certificates bash

COPY --from=build-env /build/bin /app/bin
COPY --from=build-env /build/Makefile /app/
COPY --from=build-env /build/configuration /app/configuration

WORKDIR /app
EXPOSE 8101
ENTRYPOINT ["/app/bin/service"]
