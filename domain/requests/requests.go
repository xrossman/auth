package requests

import (
	"github.com/google/uuid"

	"gitlab.com/apartstech/layer2/auth.git/v1/common"
	"gitlab.com/apartstech/layer2/auth.git/v1/domain/models"
	"gitlab.com/apartstech/proto.git/pkg/models/aclmodels"
)

type SubscribeRequestMessage struct {
	Email string `json:"email" validate:"required,email"`
}

type LoginRequestMessage struct {
	Login    string `json:"login" validate:"required"`
	Password string `json:"password" validate:"required"`
}

type RenewTokenRequestMessage struct {
	Token string `json:"token" validate:"required"`
}

type SignUpStepOneRequestMessage struct {
	Email                string `json:"email" validate:"required,email"`
	Password             string `json:"password" validate:"required"`
	PasswordConfirmation string `json:"password_confirmation" validate:"required,eqfield=Password"`
}

type UpdateUserRequestMessage struct {
	UUID uuid.UUID       `json:"uuid" validate:"required"`
	User *aclmodels.User `json:"user" validate:"required"`
}

type ReadUserRequestMessage struct {
	Login string `json:"login" validate:"required"`
}

type CreateUserRequestMessage struct {
	User *models.User `json:"user" validate:"required"`
}

type ListUsersRequestMessage struct {
	Pagination *common.Pagination `json:"pagination"`
}

type CreateUserDataRequestMessage struct {
	UserId int64            `json:"user_id"`
	Data   *models.UserData `json:"data"`
}

type UpdateUserDataRequestMessage struct {
	UserId   int64            `json:"user_id"`
	UserData *models.UserData `json:"user_data"`
}

type ReadUserDataRequestMessage struct {
	UserId int64 `json:"user_id"`
}
