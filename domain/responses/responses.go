package responses

import (
	"time"

	"gitlab.com/apartstech/layer2/auth.git/v1/common"
	"gitlab.com/apartstech/layer2/auth.git/v1/domain/models"
)

type LoginResponseMessage struct {
	TokenData TokenData    `json:"token_data"`
	User      *models.User `json:"user"`
}

type TokenData struct {
	Token      string    `json:"token"`
	RenewToken string    `json:"renew_token"`
	ExpiredAt  time.Time `json:"expired_at"`
}

type RenewTokenResponseMessage struct {
	TokenData TokenData `json:"token_data"`
}

type SignUpResponseMessage struct {
	LoginResponseMessage
}

type SignUpStepOneResponseMessage struct {
	Code string `json:"code"`
}

type StatusResponseMessage struct {
	Status bool `json:"status"`
}

type ListUsersResponseMessage struct {
	Users      []*models.User     `json:"Users"`
	Pagination *common.Pagination `json:"Pagination"`
}

type ReadUserDataResponseMessage struct {
	Data *models.UserData `json:"data"`
}
