package models

import (
	"time"

	"github.com/golang/protobuf/ptypes/wrappers"
	"github.com/google/uuid"
)

type Gender string

const (
	FemaleGender = Gender("female")
	MaleGender   = Gender("male")
)

type User struct {
	UUID          uuid.UUID  `json:"uuid"`
	Email         string     `json:"email" validate:"required,email"`
	Phone         *string    `json:"phone"`
	Password      string     `json:"-"`
	LastLoginDate *time.Time `json:"last_login_date"`
	CreatedAt     time.Time  `json:"created_at"`
	UpdatedAt     time.Time  `json:"updated_at"`
	DeletedAt     *time.Time `json:"deleted_at"`
}

type UserData struct {
	ID            int                   `json:"id"`
	Birthday      *time.Time            `json:"birthday"`
	FirstName     *string               `json:"first_name"`
	MiddleName    *string               `json:"middle_name"`
	LastName      *string               `json:"last_name"`
	Gender        *Gender               `json:"gender"`
	MaritalStatus *wrappers.StringValue `json:"marital_status"`
	CreatedAt     time.Time             `json:"created_at"`
	UpdatedAt     *time.Time            `json:"updated_at"`
}

type Subscription struct {
	ID        uint      `json:"-"`
	Email     string    `json:"email"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"-"`
}

func (s Subscription) TableName() string {
	return "subscribes"
}

func NewSubscription(email string) *Subscription {
	return &Subscription{
		Email: email,
	}
}
