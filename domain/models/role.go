package models

type Role struct {
	ID    int64     `json:"id"`
	Name  string    `json:"name"`
	Desc  string    `json:"desc"`
	Level RoleLevel `json:"level"`
}

type RoleLevel int32

const (
	GuestRoleLevel         RoleLevel = 0
	UserRoleLevel          RoleLevel = 1
	RealtorRoleLevel       RoleLevel = 2
	AdministratorRoleLevel RoleLevel = 3
)
