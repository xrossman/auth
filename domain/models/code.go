package models

import "time"

type Code struct {
	ID         uint
	Value      string
	AuthDataID uint
	AuthData   AuthData
	CreatedAt  time.Time
}
