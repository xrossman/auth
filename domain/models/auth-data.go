package models

import "time"

type AuthData struct {
	ID           uint
	Login        string
	PasswordHash string
	CreatedAt    time.Time
}
