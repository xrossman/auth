package apartflow

const (
	UpdateApartmentRequestCtxValue = "updateApartmentRequest"
	ApartmentUpdateStatusCtxValue  = "apartmentUpdateStatus"
	ApartmentIDCtxValue            = "apartmentID"
	ApartmentCtxValue              = "apartment"
	RealtorIDCtxValue              = "realtorID"
)
