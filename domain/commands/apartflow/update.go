package apartflow

import (
	"fmt"

	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/grpc"
	"gitlab.com/apartstech/proto.git/pkg/models/apartmentsmodels"
	"gopkg.in/workanator/go-floc.v2"
)

func UpdateApartment(ctx floc.Context, ctrl floc.Control) error {
	var (
		req *apartmentsmodels.UpdateApartmentRequest
		ok  bool
	)

	if req, ok = ctx.Value(UpdateApartmentRequestCtxValue).(*apartmentsmodels.UpdateApartmentRequest); !ok {
		return fmt.Errorf("can't get apartment update equest from context")
	}

	res, err := grpc.GetApartmentsClient().UpdateApartment(ctx.Ctx(), req)
	if err != nil {
		return err
	}

	ctrl.Complete(res)

	return nil
}
