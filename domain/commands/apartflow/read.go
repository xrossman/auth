package apartflow

import (
	"fmt"

	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/grpc"
	"gitlab.com/apartstech/proto.git/pkg/models/apartmentsmodels"
	"gopkg.in/workanator/go-floc.v2"
)

func ReadApartmentByID(ctx floc.Context, ctrl floc.Control) error {
	var (
		apartmentID uint64
		ok          bool
	)

	if apartmentID, ok = ctx.Value(ApartmentIDCtxValue).(uint64); !ok {
		return fmt.Errorf("can't get apartment ID from context")
	}

	res, err := grpc.GetApartmentsClient().ReadApartment(ctx.Ctx(), &apartmentsmodels.ReadApartmentByIDRequest{
		ID: apartmentID,
	})
	if err != nil {
		return err
	}

	ctx.AddValue(ApartmentCtxValue, res.Apartment)

	return nil
}
