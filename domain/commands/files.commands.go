package commands

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"time"

	"gitlab.com/apartstech/layer2/auth.git/v1/cfg"
	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/grpc"
	"gitlab.com/apartstech/proto.git/pkg/models/aclmodels"
	"gitlab.com/apartstech/proto.git/pkg/models/filesmodels"
	"golang.org/x/sync/errgroup"
)

type ErrNodata string

func (e ErrNodata) Error() string {
	return "no data in response"
}

func SaveImage(data []byte, category string) (string, error) {
	fileName, err := sendFile(data, category)
	if err != nil {
		return "", err
	}

	return fileName, nil
}

func SetAvatar(data []byte, user *aclmodels.User) (string, error) {
	var oldAvatar string
	if user.AdditionalData.Avatar != "" {
		oldAvatar = user.AdditionalData.Avatar
	}
	fileName, err := sendFile(data, filesmodels.FileCategory_avatar.String())
	if err != nil {
		return "", err
	}

	user.AdditionalData.Avatar = fileName
	c := grpc.GetACLClient()

	_, err = c.UserUpdate(context.Background(), &aclmodels.UserRequest{
		UUID:        user.UUID,
		Login:       user.Email,
		DisplayName: user.DisplayName,
		User:        user,
	})

	gp := errgroup.Group{}
	if err != nil {
		gp.Go(func() error {
			return DeleteImage(filesmodels.FileCategory_avatar.String(), fileName)
		})
		return "", err
	}

	if oldAvatar != "" {
		go DeleteImage(filesmodels.FileCategory_avatar.String(), oldAvatar)
	}

	return fileName, nil
}

func DeleteImage(category, name string) error {
	var (
		fileCategory int32
		ok           bool
	)

	if fileCategory, ok = filesmodels.FileCategory_value[category]; !ok {
		return fmt.Errorf("not supported file category name %s", category)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	_, err := grpc.GetFilesClient().Delete(ctx, &filesmodels.DeleteFileRequest{
		Name:     name,
		Category: filesmodels.FileCategory(fileCategory),
	})

	return err
}

func sendFile(data []byte, category string) (string, error) {
	cl := grpc.GetFilesClient()

	ctx, cancel := context.WithTimeout(context.Background(), cfg.Store.WS.ContextTimeout)
	defer cancel()

	stream, err := cl.Upload(ctx)
	if err != nil {
		return "", err
	}

	var (
		ok           bool
		fileCategory int32
	)

	if fileCategory, ok = filesmodels.FileCategory_value[category]; !ok {
		return "", fmt.Errorf("not supported file category name %s", category)
	}

	msg := &filesmodels.UploadRequest{
		Data: &filesmodels.UploadRequest_Info{
			Info: &filesmodels.FileInfo{
				Category: filesmodels.FileCategory(fileCategory),
			},
		},
	}

	if err := stream.Send(msg); err != nil {
		return "", err
	}

	r := bytes.NewReader(data)
	buf := make([]byte, 1024)

	for {
		n, err := r.Read(buf)
		if err == io.EOF {
			break
		}
		if err != nil {
			return "", err
		}

		msg := &filesmodels.UploadRequest{
			Data: &filesmodels.UploadRequest_Chunks{
				Chunks: buf[:n],
			},
		}

		if err := stream.Send(msg); err != nil {
			return "", err
		}
	}

	res, err := stream.CloseAndRecv()
	if err != nil {
		return "", err
	}

	return res.Name, nil
}
