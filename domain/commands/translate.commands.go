package commands

import (
	"fmt"
	"sync"

	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/translations"
)

func TranslateTextCommand(to, text string) (string, error) {
	return translations.Translate(to, text)
}

func translateMany(source []map[string]string) error {
	type translateResult struct {
		idx        int
		targetLang string
		result     string
	}
	wg := &sync.WaitGroup{}
	resultCh := make(chan translateResult)
	errCh := make(chan error)
	stopCh := make(chan struct{})
	for i, ss := range source {
		if len(ss) == 0 {
			return fmt.Errorf("nothing to translate")
		}

		targets := translations.DetectMissingLanguages(ss)
		if len(targets) == 0 {
			continue
		}

		for _, text := range ss {
			for _, target := range targets {
				wg.Add(1)
				go func(resultCh chan translateResult, errCh chan error, i int, target, text string) {
					result, err := TranslateTextCommand(target, text)
					if err != nil {
						errCh <- err
						return
					}

					resultCh <- translateResult{
						idx:        i,
						targetLang: target,
						result:     result,
					}
					wg.Done()
				}(resultCh, errCh, i, target, text)
			}
		}
	}

	go func() {
		wg.Wait()
		stopCh <- struct{}{}
	}()

	for {
		select {
		case r := <-resultCh:
			source[r.idx][r.targetLang] = r.result
		case err := <-errCh:
			resultCh = nil
			errCh = nil
			return err
		case <-stopCh:
			return nil
		}
	}
}
