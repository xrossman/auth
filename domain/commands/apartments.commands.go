package commands

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/apartstech/layer2/auth.git/v1/domain/commands/apartflow"
	"gitlab.com/apartstech/layer2/auth.git/v1/domain/commands/mapsflow"
	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/grpc"
	"gitlab.com/apartstech/proto.git/pkg/models"
	"gitlab.com/apartstech/proto.git/pkg/models/apartmentsmodels"
	"gitlab.com/apartstech/proto.git/pkg/models/filesmodels"
	"gitlab.com/apartstech/proto.git/pkg/models/financialmodels"
	"gitlab.com/apartstech/proto.git/pkg/models/mapsmodels"
	"gopkg.in/workanator/go-floc.v2"
	"gopkg.in/workanator/go-floc.v2/run"
)

func DeleteImageFromApartment(name string, apartmentID uint64) error {
	c := grpc.GetApartmentsClient()
	msg := &apartmentsmodels.DeleteImageRequest{
		ApartmentID: apartmentID,
		Name:        name,
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	_, err := c.DeleteImage(ctx, msg)

	return err
}

func AddImageToApartment(data []byte, entityID uint64) (string, error) {
	fileName, err := sendFile(data, filesmodels.FileCategory_apartment.String())
	if err != nil {
		return "", err
	}

	c := grpc.GetApartmentsClient()
	msg := &apartmentsmodels.AddImageNameToApartmentRequest{
		ImageName:   fileName,
		ApartmentID: entityID,
	}

	_, err = c.AddImage(context.Background(), msg)
	if err != nil {
		go DeleteImage(filesmodels.FileCategory_apartment.String(), fileName)
		return "", err
	}

	return fileName, nil
}

func UpdateApartmentCommand(req *apartmentsmodels.UpdateApartmentRequest) (*apartmentsmodels.ReadApartmentResponse, error) {
	if req.GetForPublish() {
		if err := addMissingTranslations(req.Apartment); err != nil {
			return nil, err
		}
	}

	c := floc.NewContext()
	ctrl := floc.NewControl(c)

	c.AddValue(apartflow.UpdateApartmentRequestCtxValue, req)
	c.AddValue(apartflow.RealtorIDCtxValue, req.Apartment.RealtorID)
	c.AddValue(apartflow.ApartmentIDCtxValue, req.ApartmentID)
	c.AddValue(mapsflow.NewApartGeoCtxValue, req.Apartment.Geo)

	fl := run.Sequence(
		mapsflow.ReadGeoByApartmentID,
		run.If(
			mapsflow.HasToCreateGeo, mapsflow.CreateApartGeo,
		),
		run.If(
			mapsflow.HasToUpdateGeo, mapsflow.UpdateApartmentGeo,
		),
		apartflow.UpdateApartment,
	)

	_, data, err := floc.RunWith(c, ctrl, fl)
	if err != nil {
		return nil, err
	}

	if res, ok := data.(*apartmentsmodels.ReadApartmentResponse); ok {
		return res, nil
	}

	return nil, fmt.Errorf("unhandled error")
}

func addMissingTranslations(apartment *apartmentsmodels.Apartment) error {
	return translateMany([]map[string]string{apartment.Name, apartment.Description})
}

func ListApartmentsCommand(filter *apartmentsmodels.ApartmentsFilter, p *models.OffsetLimitPagination) (*apartmentsmodels.ListApartmentsResponse, error) {
	ok, err := getNearestApartmentsIDs(filter)
	if err != nil {
		return nil, err
	}

	if !ok {
		return &apartmentsmodels.ListApartmentsResponse{}, nil
	}

	result, err := grpc.GetApartmentsClient().ListApartments(context.Background(), apartmentsmodels.NewListApartmentRequest(p, filter))
	if err != nil {
		return nil, err
	}

	//ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	//
	//defer cancel()
	//
	//g := errgroup.Group{}
	//for i, a := range result.Apartments {
	//	g.Go(
	//		func(i int) func() error {
	//			return func() error {
	//				a.Apartment.Prices, err = updateApartmentPrices(ctx, result.Apartments[i].Apartment.Prices)
	//				if err != nil {
	//					return err
	//				}
	//
	//				result.Apartments[i] = a
	//
	//				return nil
	//			}
	//		}(i))
	//}
	//
	//err = g.Wait()
	//if err != nil {
	//	return nil, err
	//}

	return result, nil
}

func ReadApartmentByIDCommand(apartmentID uint64) (*apartmentsmodels.ReadApartmentResponse, error) {
	m := &apartmentsmodels.ReadApartmentByIDRequest{
		ID: apartmentID,
		Filter: &apartmentsmodels.ApartmentsFilter{
			ExcludeStatuses: []string{apartmentsmodels.ApartmentStatus_blocked.String()},
		},
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)

	defer cancel()

	result, err := grpc.GetApartmentsClient().ReadApartment(ctx, m)
	if err != nil {
		return nil, err
	}

	result.Apartment.Prices, err = updateApartmentPrices(ctx, result.Apartment.Prices)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func SearchMetaDataCommand(filter *apartmentsmodels.ApartmentsFilter) (*apartmentsmodels.SearchMetaDataResponse, error) {
	ok, err := getNearestApartmentsIDs(filter)
	if err != nil {
		return nil, err
	}

	if !ok {
		return &apartmentsmodels.SearchMetaDataResponse{}, nil
	}

	return grpc.GetApartmentsClient().SearchMetaData(context.Background(), apartmentsmodels.NewListApartmentRequest(nil, filter))
}

func SetPropertyStatus(id uint64, status string) (*models.StatusResponse, error) {
	st, err := apartmentsmodels.NewApartmentStatusFromString(status, nil)
	if err != nil {
		return nil, err
	}

	message := &apartmentsmodels.SetApartmentStatusRequest{
		ApartmentID: id,
		Status:      st,
	}

	return grpc.GetApartmentsClient().SetApartmentStatus(context.Background(), message)
}

func getNearestApartmentsIDs(filter *apartmentsmodels.ApartmentsFilter) (bool, error) {
	var (
		nearest *mapsmodels.FindNearestApartmentsResponse
		err     error
	)

	if filter.Geo == nil {
		return true, nil
	}

	nearest, err = grpc.GetMapsClient().FindNearestApartments(context.Background(), &mapsmodels.FindNearestApartmentsRequest{
		Filter:            &mapsmodels.FindNearestApartmentsRequest_Geo{Geo: filter.Geo},
		RadiusCoefficient: *filter.RadiusRate,
	})
	if err != nil {
		return false, err
	}

	if nearest != nil {
		if nearest.ApartmentsIDs == nil {
			return false, nil
		}

		filter.IDs = nearest.ApartmentsIDs
	}

	return true, nil
}

func updateApartmentPrices(ctx context.Context, prices []apartmentsmodels.ApartmentPrice) ([]apartmentsmodels.ApartmentPrice, error) {
	if prices == nil {
		return nil, nil
	}

	symbs := apartmentsmodels.GetMissingCurrencies(prices)
	if symbs != nil {
		rates, err := grpc.GetFinanceClient().ConvertMany(ctx, &financialmodels.ConvertManyRequest{
			Base:    prices[0].Currency,
			Symbols: symbs,
			Amount:  *prices[0].Amount,
		})
		if err != nil {
			return prices, err
		}

		for curr, amount := range rates.Result {
			p := apartmentsmodels.ApartmentPrice{
				Currency:   curr,
				Amount:     &amount,
				Calculated: true,
			}

			prices = append(prices, p)
		}
	}

	return prices, nil
}
