package mapsflow

import (
	"fmt"

	"gitlab.com/apartstech/layer2/auth.git/v1/domain/commands/apartflow"
	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/grpc"
	"gitlab.com/apartstech/proto.git/pkg/models/mapsmodels"
	"gopkg.in/workanator/go-floc.v2"
)

func CreateApartGeo(ctx floc.Context, control floc.Control) error {
	var (
		apartmentID uint64
		realtorID   string
		geo         *mapsmodels.Geo
		ok          bool
	)

	if geo, ok = ctx.Value(NewApartGeoCtxValue).(*mapsmodels.Geo); !ok {
		return fmt.Errorf("can't get apartment geo value from context")
	}

	if apartmentID, ok = ctx.Value(apartflow.ApartmentIDCtxValue).(uint64); !ok {
		return fmt.Errorf("can't get apartment ID value from context")
	}

	if realtorID, ok = ctx.Value(apartflow.RealtorIDCtxValue).(string); !ok {
		return fmt.Errorf("can't get realtor ID value from context")
	}

	_, err := grpc.GetMapsClient().StoreApartmentAddress(ctx.Ctx(), &mapsmodels.StoreApartmentAddressRequest{
		ApartmentID: apartmentID,
		RealtorID:   realtorID,
		Geo:         geo,
	})
	if err != nil {
		return err
	}

	ctx.AddValue(ExistedApartGeoCtxValue, geo)
	return nil
}

func HasToCreateGeo(ctx floc.Context) bool {
	if g, ok := ctx.Value(ExistedApartGeoCtxValue).(*mapsmodels.Geo); ok {
		if n, ok := ctx.Value(NewApartGeoCtxValue).(*mapsmodels.Geo); ok {
			if g == nil && n == nil {
				return false
			}
		}

		if g != nil {
			return false
		}
	}

	return true
}
