package mapsflow

import (
	"fmt"

	"gitlab.com/apartstech/layer2/auth.git/v1/domain/commands/apartflow"
	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/grpc"
	"gitlab.com/apartstech/proto.git/pkg/models/apartmentsmodels"
	"gitlab.com/apartstech/proto.git/pkg/models/mapsmodels"
	"gopkg.in/workanator/go-floc.v2"
)

func UpdateApartmentGeo(ctx floc.Context, ctrl floc.Control) error {
	var (
		req *apartmentsmodels.UpdateApartmentRequest
		ok  bool
	)

	if req, ok = ctx.Value(apartflow.UpdateApartmentRequestCtxValue).(*apartmentsmodels.UpdateApartmentRequest); !ok {
		return fmt.Errorf("can't get apartment update request value from context")
	}

	res, err := grpc.GetMapsClient().UpdateApartmentGeo(ctx.Ctx(), &mapsmodels.StoreApartmentAddressRequest{
		Geo:         req.Apartment.Geo,
		ApartmentID: req.ApartmentID,
		RealtorID:   req.Apartment.RealtorID,
	})
	if err != nil {
		return err
	}

	ctx.AddValue(GeoUpdateStatus, res.Status)

	return nil
}

func HasToUpdateGeo(ctx floc.Context) bool {
	var (
		req *apartmentsmodels.UpdateApartmentRequest
		geo *mapsmodels.Geo
		ok  bool
	)

	if req, ok = ctx.Value(apartflow.UpdateApartmentRequestCtxValue).(*apartmentsmodels.UpdateApartmentRequest); !ok {
		return false
	}

	if geo, ok = ctx.Value(ExistedApartGeoCtxValue).(*mapsmodels.Geo); !ok {
		return false
	}

	return !geo.Equal(req.Apartment.Geo)
}
