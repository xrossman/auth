package mapsflow

import (
	"context"
	"fmt"

	"gitlab.com/apartstech/layer2/auth.git/v1/domain/commands/apartflow"
	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/grpc"
	"gitlab.com/apartstech/proto.git/pkg/models/mapsmodels"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gopkg.in/workanator/go-floc.v2"
)

func ReadGeoByApartmentID(ctx floc.Context, ctrl floc.Control) error {
	var (
		apartmentID uint64
		geo         *mapsmodels.Geo
		ok          bool
	)

	if apartmentID, ok = ctx.Value(apartflow.ApartmentIDCtxValue).(uint64); !ok {
		return fmt.Errorf("can't get apartment ID from conttext")
	}

	res, err := grpc.GetMapsClient().ReadApartmentsGeo(context.Background(), &mapsmodels.ReadApartmentsGeoRequest{
		ApartmentsIDs: []uint64{apartmentID},
	})

	if err != nil {
		if st, ok := status.FromError(err); ok {
			if st.Code() == codes.NotFound {
				ctx.AddValue(ExistedApartGeoCtxValue, geo)
				return nil
			}
		}

		return err
	}

	ctx.AddValue(ExistedApartGeoCtxValue, res.Result[apartmentID])

	return nil
}
