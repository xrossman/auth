package commands

import (
	"context"

	"gitlab.com/apartstech/layer2/auth.git/v1/pkg/grpc"
	"gitlab.com/apartstech/proto.git/pkg/models/financialmodels"
)

func ConvertSingleCurrencyCommand(req *financialmodels.ConvertSingleCurrencyRequest) (*financialmodels.ConvertSingleCurrencyResponse, error) {
	return grpc.GetFinanceClient().ConvertSingle(context.Background(), req)
}
