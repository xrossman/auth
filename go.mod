module gitlab.com/apartstech/layer2/auth.git/v1

go 1.16

require (
	cloud.google.com/go v0.81.0
	github.com/gin-contrib/cors v1.3.0
	github.com/gin-contrib/logger v0.0.2
	github.com/gin-gonic/gin v1.6.2
	github.com/go-http-utils/headers v0.0.0-20181008091004-fed159eddc2a
	github.com/golang/protobuf v1.5.1
	github.com/google/uuid v1.1.2
	github.com/gorilla/sessions v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/kr/pretty v0.2.0 // indirect
	github.com/markbates/goth v1.61.1
	github.com/microparts/errors-go-gin v0.4.2-0.20200517160140-51cd3f30cefa
	github.com/rs/zerolog v1.18.0
	github.com/spacetab-io/configuration-go v1.1.0
	github.com/stretchr/testify v1.7.0
	gitlab.com/apartstech/infrastructure.git v0.1.0
	gitlab.com/apartstech/layer2/notifications.git v0.1.0
	gitlab.com/apartstech/proto.git v0.18.1
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	golang.org/x/text v0.3.5
	google.golang.org/api v0.43.0
	google.golang.org/grpc v1.36.1
	gopkg.in/go-playground/validator.v9 v9.30.0
	gopkg.in/workanator/go-floc.v2 v2.0.0-20180116071336-154d662ee01d
	gopkg.in/yaml.v2 v2.3.0
)

replace github.com/microparts/errors-go-gin v0.4.2-0.20200517160140-51cd3f30cefa => github.com/zeroabe/errors-go-gin v0.4.2-0.20200517162620-353887d1cb6a
